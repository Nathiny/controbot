# How to compile controbot?

If you have a Java JRE but no JDK or do not want to bother with the dependencies, you can compile the program using docker and run the jar file using your JRE.

## Using docker

- Dependency:

  - `docker`

- Optional dependency (if you want to run the program outside docker):
  - `jre-openjdk>=11` 

- If you want to run the program outside docker, replace the Java version in the `pom.xml` to your JRE version by replacing all occurrences of `<11>` or `<13>` to your JDK version (tested for `<11>` and `<13>`).

- See all possibilities of the script using

  > ./docker/make.sh help

- Compile the project using
	
	> ./docker/make.sh

- Create a executable jar using
	
	> ./docker/make.sh jar

- Clean the target folder using
	
	> ./docker/make.sh clean

- Check the code with checkstyle and generate the javadoc using
	
	> ./docker/make.sh check

- Run the program inside a docker container (useful if you do not have a JRE installed) using
	
	> ./docker/make.sh run

## Without docker

- Dependencies:
  - `maven`
  - `jdk-openjdk>=11`
  - `java-openjfx>=11`
  - `ivy-java>=1.2.18`
  - `gson-extras>=2.8.5`

- Optional dependency (to generate UML graphs):
  - `graphviz>=2.44`

### Installing the necessary Java libraries

- Download the two next jar files

  - [https://www.eei.cena.fr/products/ivy/download/packages/ivy-java-full-1.2.18.jar](https://www.eei.cena.fr/products/ivy/download/packages/ivy-java-full-1.2.18.jar)

  - [https://artifactory.cronapp.io/public-release/com/google/code/gson/gson-extras/2.8.5/gson-extras-2.8.5.jar](https://artifactory.cronapp.io/public-release/com/google/code/gson/gson-extras/2.8.5/gson-extras-2.8.5.jar)

- Install the libraries using

  > mvn install:install-file -Dfile=gson-extras-2.8.5.jar -DgroupId=com.google.code.gson -DartifactId=gson-extras -Dversion=2.8.5 -Dpackaging=jar -DgeneratePom=true

  > mvn install:install-file -Dfile=ivy-java-full-1.2.18.jar -DgroupId=fr.dgac -DartifactId=Ivy -Dversion=1.2.18 -Dpackaging=jar -DgeneratePom=true

### Compile the project

- Replace the Java version in the `pom.xml` to your Java version by replacing all occurrences of `<11>` or `<13>` to your JDK version (tested for `<11>` and `<13>`).

- See all possibilities of the script using

  > ./make.sh help

- Compile the project using

  > ./make.sh compile

- Create an executable jar using

  > ./make.sh jar

- Clean the target folder using

  > ./make.sh clean

- Check the code with checkstyle and generate the javadoc using

  > ./make.sh check

# Run the program

- Run the executable jar using

  - > ./make.sh run

  - Or the launch the jar in the `target` folder