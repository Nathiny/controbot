#!/bin/bash

print_help () {
    printf "Syntax: ./docker/make.sh [OPTION]\n\n"
    printf "Without option:\t\tCompiles the project.\n"
    printf "Options:\n"
    printf "\tclean\t\tCleans the target directory.\n"
    printf "\tcompile\t\tCompiles the project.\n"
    printf "\tjar\t\tCompiles the project and makes a jar file with all the dependencies.\n"
    printf "\trun\t\tRun the jar file in the target folder.\n"
    printf "\tcheck\t\tRun the checkstyle and generates the javadoc of the project.\n"
    printf "\tcount\t\tCounts the numbers of lines in each files and in total.\n"
    printf "\tbash\t\tOpens a bash terminal inside the docker container.\n"
    printf "\ttest\t\tRuns the JUnit tests.\n"
    printf "\treport\t\tOpens project's Javadoc and Checkstyle in default web-browser.\n"
}


if (( $# >= 1 ))
then
    if [ $1 = "clean" ]
    then
        arg="clean"
    elif [ $1 = "jar" ]
    then
        arg="jar"
    elif [ $1 = "compile" ]
    then
        arg="compile"
    elif [ $1 = "check" ]
    then
        arg="check"
    elif [ $1 = "count" ]
    then
        arg="count"
    elif [ $1 = "test" ]
    then
        arg="test"
    elif [ $1 = "bash" ]
    then
        arg="bash"
    elif [ $1 = "report" ]
    then
        browser=$(xdg-settings get default-web-browser)
        if [ -f "target/site/project-reports.html" ]
        then
            $browser target/site/project-reports.html
            exit 0
        else
            printf "Generate report using: \n\t./docker/make.sh check\n"
            exit 1
        fi
    elif [ $1 = "run" ]
    then
        ./docker/run.sh
        exit 0
    else
        echo "Invalid argument."
        print_help
        exit -1
    fi
else
    arg="compile"
fi


if [ $arg = "bash" ]
then
    sudo docker run --entrypoint $arg -it --rm -v $(pwd):/home/code \
        zpex/controbot-dev:latest
else
    sudo docker run -it --rm -v $(pwd):/home/code \
        zpex/controbot-dev:latest \
        $arg
fi

if [ -d "target" ]
then
    sudo chown -R $USER target
fi
