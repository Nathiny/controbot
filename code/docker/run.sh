# check if running on Linux or OSX
UNAME=$(uname -s)

############################################################
# grant access to X-Server
############################################################
if [ $UNAME == "Linux" ]; then
    XSOCK=/tmp/.X11-unix
    XAUTH=/tmp/.docker.xauth
    touch $XAUTH
    xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

    # options to grant access to the Xserver
    X_WINDOW_OPTS="--volume=$XSOCK:$XSOCK --volume=$XAUTH:$XAUTH --env=XAUTHORITY=${XAUTH} --env=DISPLAY=${DISPLAY}"
fi

# using xauth with docker on OSX doesn't work, so we use socat:
# see https://github.com/docker/docker/issues/8710
if [ $UNAME == "Darwin" ]; then
    X_WINDOW_OPTS="--env=DISPLAY=192.168.99.1:0"
    TCPPROXY="socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\""
fi


############################################################
# Audio
############################################################
if [ $UNAME == "Linux" ]; then
    # pass audio to pulseaudio server on host
    USER_UID=$(id -u)
    PULSE_AUDIO_OPTS="--volume=/run/user/${USER_UID}/pulse:/run/pulse"
fi



# run the docker container with all the fancy options
sudo docker run \
    ${X_WINDOW_OPTS} \
    -v $(pwd):/home/controbot/code \
    ${PULSE_AUDIO_OPTS} \
    -e LOCAL_USER_ID=`id -u` \
    -e LOCAL_GROUP_ID=`id -g`\
    --rm -it zpex/controbot-jre:latest \
    java -jar target/controbot*




# remember exit status
EXIT_STATUS=$?

############################################################
# cleanup after exiting from docker container
############################################################

# cleanup XAUTHORITY file again
rm -f $XAUTH

# on OSX kill background socat process again
if [ $UNAME == "Darwin" ]; then
    pkill -f "$TCPPROXY"
fi
