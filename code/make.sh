#!/bin/bash

PROJECT_NAME=controbot

make_project () {
    mvn compile
}

make_jar () {
    mvn compile
    mvn assembly:single
}

remove_trailing_spaces () {
    find . -type f -name "*.java" -print0 | xargs -0 sed -i 's/\s\+$//'
    find . -type f -name "*.java" -print0 | xargs -0 sed -i -e '$a\'
}

count () {
    find . -type f -name '*.java' -print0 | xargs -0 wc > uber
    sort -k 1 -n uber
    rm uber
}

print_help () {
    printf "Syntax: ./make.sh [OPTION]\n\n"
    printf "Without option:\t\tCompiles the project.\n"
    printf "Options:\n"
    printf "\tclean\t\tCleans the target directory.\n"
    printf "\tcompile\t\tCompiles the project.\n"
    printf "\tjar\t\tCompiles the project and makes a jar file with all the dependencies.\n"
    printf "\trun\t\tRun the jar file in the target folder.\n"
    printf "\tcheck\t\tRun the checkstyle and generates the javadoc of the project.\n"
    printf "\tcount\t\tCounts the numbers of lines in each files and in total.\n"
    printf "\ttest\t\tRuns the JUnit tests.\n"
    printf "\treport\t\tOpens project's Javadoc and Checkstyle in default web-browser.\n"
}

remove_trailing_spaces

if (( $# >= 1 ))
then
    if [ $1 = "clean" ]
    then
        mvn clean
        echo Folder cleaned.
    elif [ $1 = "jar" ]
    then
        make_jar
    elif [ $1 = "run" ]
    then
        cd target
        java -jar $(ls $PROJECT_NAME*)
    elif [ $1 = "compile" ]
    then
        mvn compile
    elif [ $1 = "check" ]
    then
        mvn site
    elif [ $1 = "count" ]
    then
        count 
    elif [ $1 = "test" ]
    then
        mvn test 
    elif [ $1 = "report" ]
    then
        browser=$(xdg-settings get default-web-browser)
        if [ -f "target/site/project-reports.html" ]
        then
            $browser target/site/project-reports.html
        else
            printf "Generate report using: \n\t./make.sh check\n"
        fi
    else
        printf "Invalid argument.\n\n"
        print_help
        exit -1
    fi
else
    mvn javafx:run
fi
