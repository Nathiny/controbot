package fr.enac.sita.controbot;

import fr.enac.sita.controbot.view.View;
import javafx.application.Application;

public abstract class Main {

    private static void launchView() {
        Application.launch(View.class, "ViewName");
    }

    /**
     * Start the programme controbot (No args needed).
     *
     * @param args No args needed.
     */
    public static void main(final String[] args) {
        launchView();
    }
}
