package fr.enac.sita.controbot.actions;

import java.util.List;

/**
 * Action modelizes different actions like a movement or a change of state of an
 * actuator.
 */
public class Action {
    /** Id of the action. */
    private String id;
    /**
     * When defines when the action should be done. Takes two values "after" or
     * "while" and <otherActionId>. The init takes only one real argument "init"
     * followed by "". ex: { "id": "init", "when": [ "init", "" ], ... } { "id":
     * "setServo", "when": [ "after", "init" ], ... } After the "init" action,
     * "setServo" will start.
     */
    private List<String> when;
    /**
     * Action defines the Ivy message (often followed by params). ex: "action":
     * "servo1 set_angle", "params": [ "90" ].
     */
    private String action;
    /**
     * Params of the action for the Ivy message ex: "action": "go_to", "params": [
     * "2000", "1000", "90" ].
     */
    private List<String> params;

    /**
     * Returns the id of the Action.
     *
     * @return The id of the Action.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id of the Action.
     *
     * @param id Id of the Action.
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Returns when the action must be started.
     *
     * @return The list when.
     */
    public List<String> getWhen() {
        return when;
    }

    /**
     * Sets when the action must be started.
     *
     * @param when The list when.
     */
    public void setWhen(final List<String> when) {
        this.when = when;
    }

    /**
     * Returns the action.
     *
     * @return The action.
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the action.
     *
     * @param action The action.
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * Returns the parameters of the action.
     *
     * @return The params.
     */
    public List<String> getParams() {
        return params;
    }

    /**
     * Sets the parameters of the action.
     *
     * @param params Parameters for the action.
     */
    public void setParams(final List<String> params) {
        this.params = params;
    }

}
