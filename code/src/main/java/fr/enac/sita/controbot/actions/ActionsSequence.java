package fr.enac.sita.controbot.actions;

import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fr.enac.sita.controbot.communication.IvyRxTx;

/**
 * ActionsSequence implements Runnable, represents a sequence of actions.
 */
public class ActionsSequence implements Runnable {
    /** HashMap of <Unique id of the action>, <action>. */
    private final LinkedHashMap<String, Action> actions;
    /** Sequence id. */
    private final String id;
    /**
     * The starting action is set to "false" in actionFinished. The value changes to
     * "true" when it ends. Used for when ("while" or "after").
     */
    private LinkedHashMap<String, Boolean> actionFinished;
    /** Ivy agent used to send messages. */
    private final IvyRxTx ivy;
    /** Name of the robot assigned to the sequence. */
    private final String robotName;

    /**
     * Sets the action to finished state.
     *
     * @param actionId Id of the action.
     */
    public synchronized void setActionFinished(final String actionId) {
        actionFinished.put(actionId, true);
        this.notifyAll();
    }

    /**
     * Constructs a sequence.
     *
     * @param id          Id of the action.
     * @param actionsJson List of the actions in JSon.
     * @param ivy         Ivy Agent used to send the message.
     * @param robotName   Name of the robot for the sequence.
     */
    public ActionsSequence(final String id, final String actionsJson, final IvyRxTx ivy, final String robotName) {
        this.actions = new LinkedHashMap<>();
        this.ivy = ivy;
        this.robotName = robotName;
        this.id = id;
        final Gson gson = new GsonBuilder().create();

        final List<Action> actionsList = gson.fromJson(actionsJson, new TypeToken<List<Action>>() {
        }.getType());

        actionsList.forEach(action -> {
            actions.put(robotName + id + action.getId(), action);
            this.ivy.addTrajectoryListener(robotName + this.id + action.getId(),
                    (robotActionId, finished) -> setActionFinished(robotActionId));
        });

        this.actionFinished = new LinkedHashMap<>();
    }

    /** Starts the sequence. */
    public synchronized void run() {
        actionFinished = new LinkedHashMap<>();
        actions.forEach((actionId, action) -> {
            if (action.getAction().equals("go_to")) {
                actionFinished.put(actionId, false);
            } else {
                actionFinished.put(actionId, true);
            }

            if ("init".equals(action.getWhen().get(0))) {
                ivy.sendActionMessage(robotName, action.getAction(), action.getParams(), actionId);
            } else if ("after".equals(action.getWhen().get(0))) {
                while (!actionFinished.get(robotName + id + action.getWhen().get(1))) {
                    try {
                        this.wait();
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                ivy.sendActionMessage(robotName, action.getAction(), action.getParams(), actionId);
            } else if ("while".equals(action.getWhen().get(0))) {
                ivy.sendActionMessage(robotName, action.getAction(), action.getParams(), actionId);
            }

        });
    }

    /**
     * @return The name of the robot.
     */
    public String getRobotName() {
        return robotName;
    }

}
