package fr.enac.sita.controbot.communication;

/**
 * ActuatorListener is used to receive the actuator's messages.
 */
public interface ActuatorListener {
    /**
     * Used for single var-Actuator as Servo(angle), Valve(opened).
     *
     * @param robotName    The robot's name.
     * @param actuatorName The actuator's name.
     * @param var1         Parameter for the actuator's state (ex: servo's angle,
     *                     valve's state).
     */
    void onMySimpleEvent(String robotName, String actuatorName, String var1);

    /**
     * Used for double var-Actuator as Motor(speed, clockwise), Pump(power,
     * smtCatched).
     *
     * @param robotName    The robot's name.
     * @param actuatorName The actuator's name.
     * @param var1         First parameter for the actuator's state (ex: motor's
     *                     speed).
     * @param var2         Second parameter for the actuator's state (ex: motor's
     *                     direction).
     */
    void onMyDoubleEvent(String robotName, String actuatorName, String var1, String var2);
}
