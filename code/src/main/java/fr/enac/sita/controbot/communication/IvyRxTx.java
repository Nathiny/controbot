package fr.enac.sita.controbot.communication;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyClient;
import fr.dgac.ivy.IvyException;
import fr.dgac.ivy.IvyMessageListener;
import fr.enac.sita.controbot.model.robot.AbstractActuator;
import fr.enac.sita.controbot.model.robot.Motor;
import fr.enac.sita.controbot.model.robot.Pump;
import fr.enac.sita.controbot.model.robot.Servo;
import fr.enac.sita.controbot.model.robot.Valve;

/**
 * IvyRxTx is used to communicate with robots.
 */
public class IvyRxTx {

    /** Name of the IvyRxTx. */
    private String name;
    /** Bus string of the IvyRxTx. */
    private final String bus;
    /** Bus of the IvyRxTx. */
    private final Ivy busIvy;

    /** List of MovedListener. */
    private final ArrayList<MovedListener> eventMovedListeners;

    /** Hashtable of id, ActuatorListener. */
    private final Hashtable<String, ActuatorListener> eventActuatorListeners;

    /** Hashtable of id, TrajectoryListener. */
    private final Hashtable<String, TrajectoryListener> trajectoryListeners;

    /** List of TargetListener. */
    private final ArrayList<TargetListener> eventTargetListeners;

    /**
     * Constructs a IvyRxTx.
     *
     * @param name Name of the IvyRxTx.
     * @param bus  Bus of the IvyRxTx.
     */
    public IvyRxTx(final String name, final String bus) {
        this.name = name;
        this.bus = bus;
        this.busIvy = new Ivy(name, name + " is ready", null);

        this.eventMovedListeners = new ArrayList<MovedListener>();
        this.trajectoryListeners = new Hashtable<String, TrajectoryListener>();
        this.eventActuatorListeners = new Hashtable<String, ActuatorListener>();
        this.eventTargetListeners = new ArrayList<TargetListener>();

        try {
            this.busIvy.bindMsg("([a-zA-Z0-9]+) ([a-zA-Z0-9]+) angle : ([0-9]+)", onReceiveActuatorMessage);
            this.busIvy.bindMsg("([a-zA-Z0-9]+) ([a-zA-Z0-9]+) opened : (True|False)", onReceiveActuatorMessage);
            this.busIvy.bindMsg("([a-zA-Z0-9]+) ([a-zA-Z0-9]+) speed : ([0-9]+) clockwise : (True|False)",
                    onReceiveActuatorMessage);
            this.busIvy.bindMsg("([a-zA-Z0-9]+) ([a-zA-Z0-9]+) power : ([0-9]+) smtCatched : (True|False)",
                    onReceiveActuatorMessage);
            this.busIvy.bindMsg("([a-zA-Z0-9]+) trajectory_finished (True)", onTrajectoryFinished);

            this.busIvy.bindMsg("([a-zA-Z0-9]+) x ([0-9]+) y ([0-9]+) theta ([0-9]+)$", onReceivePositionMessage);
            this.busIvy.bindMsg("([a-zA-Z0-9]+) start ([0-9]+) ([0-9]+) end ([0-9]+) ([0-9]+)$",
                    onReceiveTargetMessage);
            this.busIvy.start(bus);
            System.out.println(bus);
        } catch (final IvyException ie) {
            System.out.println("Ivy error");
        }

    }

    /**
     * Adds a listener for the robot's movements.
     *
     * @param eventListener EventListener.
     */
    public void addMovedListener(final MovedListener eventListener) {
        this.eventMovedListeners.add(eventListener);
    }

    /**
     * Adds a listener for the robot's trajectory.
     *
     * @param id                 Id of the trajectory.
     * @param trajectoryListener TrajectoryListener.
     */
    public void addTrajectoryListener(final String id, final TrajectoryListener trajectoryListener) {
        this.trajectoryListeners.put(id, trajectoryListener);
    }

    /**
     * Ivy listener for reception of position.
     */
    private final IvyMessageListener onReceivePositionMessage = new IvyMessageListener() {
        @Override
        public void receive(final IvyClient arg0, final String[] args) {
            eventMovedListeners.forEach((el) -> el.onMyEvent(args[0], args[1], args[2], args[3]));
        }
    };

    /**
     * Adds a listener for the robot's target.
     *
     *
     * @param eventListener The target listener.
     */
    public void addTargetListener(final TargetListener eventListener) {
        this.eventTargetListeners.add(eventListener);
    }

    /**
     * Ivy listener for reception of target.
     */
    private final IvyMessageListener onReceiveTargetMessage = new IvyMessageListener() {

        @Override
        public void receive(final IvyClient arg0, final String[] args) {
            eventTargetListeners.forEach((tl) -> tl.onMyTargetEvent(args[0], args[1], args[2], args[3], args[4]));
        }
    };

    /**
     * Adds a listener for the actuator's state.
     *
     * @param robotName     The robot's name..
     * @param actuatorName  The actuator's name..
     * @param eventListener The actuator listener.
     */
    public void addActuatorListener(final String robotName, final String actuatorName,
            final ActuatorListener eventListener) {
        this.eventActuatorListeners.put(robotName + " " + actuatorName, eventListener);
    }

    /**
     * Ivy listener for reception of messages for actuators.
     */
    private final IvyMessageListener onReceiveActuatorMessage = new IvyMessageListener() {
        @Override
        public void receive(final IvyClient arg0, final String[] args) {
            final ActuatorListener actuListener = eventActuatorListeners.get(args[0] + " " + args[1]);
            if (args.length == 3) {
                actuListener.onMySimpleEvent(args[0], args[1], args[2]);
            } else if (args.length == 4) {
                actuListener.onMyDoubleEvent(args[0], args[1], args[2], args[3]);
            } else {
                System.out.println("Error in args onReceiveActuatorMessage Listener.");
            }
        }
    };

    /**
     * Ivy listener for reception of trajectory.
     */
    private final IvyMessageListener onTrajectoryFinished = new IvyMessageListener() {
        @Override
        public void receive(final IvyClient arg0, final String[] args) {
            final TrajectoryListener trajectoryListener = trajectoryListeners.get(args[0]);
            trajectoryListener.onMyEvent(args[0], args[1]);
        }
    };

    /**
     * Sends an Ivy message to ask for the robot's position.
     *
     * @param robotName The robot's name.
     */
    public void getPos(final String robotName) {
        try {
            busIvy.sendMsg(robotName + " get_pos");
        } catch (final IvyException ie) {
            System.out.println("can't send my get pos message on the bus");
        }
    }

    /**
     * Sends an Ivy message to ask for the actuator's state.
     *
     * @param robotName The robot's name.
     * @param actuators The list of the actuators.
     */
    public void getActuators(final String robotName, final List<AbstractActuator> actuators) {
        for (final AbstractActuator actu : actuators) {
            if (actu instanceof Servo) {
                try {
                    busIvy.sendMsg(robotName + " " + ((Servo) actu).getId() + " get_angle");
                } catch (final IvyException ie) {
                    System.out.println("can't send my get servo angle message on the bus");
                }
            } else if (actu instanceof Motor) {
                try {
                    busIvy.sendMsg(robotName + " " + ((Motor) actu).getId() + " get_speed");
                } catch (final IvyException ie) {
                    System.out.println("can't send my get motor speed message on the bus");
                }
            } else if (actu instanceof Pump) {
                try {
                    busIvy.sendMsg(robotName + " " + ((Pump) actu).getId() + " get_state");
                } catch (final IvyException ie) {
                    System.out.println("can't send my get pump state message on the bus");
                }
            } else if (actu instanceof Valve) {
                try {
                    busIvy.sendMsg(robotName + " " + ((Valve) actu).getId() + " get_state");
                } catch (final IvyException ie) {
                    System.out.println("can't send my get valve state message on the bus");
                }
            }
        }
    }

    /**
     * Send function for Ivy message to change robot's speed.
     *
     * @param robotName The robot's name.
     * @param speed     The robot's speed.
     */
    public void sendSpeed(final String robotName, final int speed) {
        try {
            busIvy.sendMsg(robotName + " set_speed " + Integer.toString(speed));
            System.out.println("set_speed " + speed);
        } catch (final IvyException ie) {
            System.out.println("can't send my speed message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change robot's rotation speed.
     *
     * @param robotName The robot's name.
     * @param omega     The robot's speed of rotation.
     */

    public void sendRotationSpeed(final String robotName, final int omega) {
        try {
            busIvy.sendMsg(robotName + " set_omega " + Integer.toString(omega));
            System.out.println("set_omega " + omega);
        } catch (final IvyException ie) {
            System.out.println("can't send my omega message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change robot's position.
     *
     * @param robotName The robot's name.
     * @param x         The robot's x position.
     * @param y         The robot's y position.
     */

    public void sendPosition(final String robotName, final int x, final int y) {
        try {
            busIvy.sendMsg(robotName + " go_to " + Integer.toString(x) + " " + Integer.toString(y));
        } catch (final IvyException ie) {
            System.out.println("can't send my position message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change robot's orientation.
     *
     * @param robotName The robot's name.
     * @param theta     The robot's theta position.
     */

    public void sendTheta(final String robotName, final int theta) {
        try {
            busIvy.sendMsg(robotName + " go_theta " + Integer.toString(theta));
        } catch (final IvyException ie) {
            System.out.println("can't send my theta message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change motor's speed.
     *
     * @param robotname The robot's name.
     * @param motorId   The motor's id.
     * @param speed     The motor's speed.
     * @param clockwise The motor's direction of rotation.
     */

    public void sendMotorCommand(final String robotname, final String motorId, final Double speed,
            final Boolean clockwise) {
        String toSend;
        try {
            final int direction = (clockwise) ? 1 : 0;
            toSend = robotname + " " + motorId + " set_clockwise " + Integer.toString(direction);
            busIvy.sendMsg(toSend);
            toSend = robotname + " " + motorId + " set_speed " + Integer.toString(speed.intValue());
            busIvy.sendMsg(toSend);
        } catch (final IvyException ie) {
            System.out.println("can't send my motor speed message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change servo's angle.
     *
     * @param robotname The robot's name.
     * @param servoId   The servo'id.
     * @param angle     The servo's angle.
     */

    public void sendServoCommand(final String robotname, final String servoId, final Double angle) {
        try {
            busIvy.sendMsg(robotname + " " + servoId + " set_angle " + Integer.toString(angle.intValue()));
            System.out.println(robotname + " " + servoId + " set_angle " + angle.intValue());
        } catch (final IvyException ie) {
            System.out.println("can't send my servo angle message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change pump's power.
     *
     * @param robotname The robot's name.
     * @param pumpId    The pump's id.
     * @param power     The pump's power.
     */

    public void sendPumpCommand(final String robotname, final String pumpId, final Double power) {
        try {
            busIvy.sendMsg(robotname + " " + pumpId + " set_power " + Integer.toString(power.intValue()));
            System.out.println(robotname + " " + pumpId + " set_power " + power.intValue());
        } catch (final IvyException ie) {
            System.out.println("can't send my pump power message on the bus");
        }
    }

    /**
     * Send function for Ivy message to change valve's state.
     *
     * @param robotname The robot's name.
     * @param valveId   Id's state.
     * @param state     State of the valve (1 for open).
     */

    public void sendValveCommand(final String robotname, final String valveId, final int state) {
        try {
            busIvy.sendMsg(robotname + " " + valveId + " set_state " + Integer.toString(state));
            System.out.println(robotname + " " + valveId + " set_state " + state);
        } catch (final IvyException ie) {
            System.out.println("can't send my valve state message on the bus");
        }
    }

    /**
     * Send message on the Ivybus.
     *
     * @param robotName  The robot's name.
     * @param actionName The action's name.
     * @param params     The parameters of the action.
     * @param actionId   The action's ID.
     */
    public void sendActionMessage(final String robotName, final String actionName, final List<String> params,
            final String actionId) {
        String paramsString = "";
        for (final String param : params) {
            paramsString = paramsString.concat(" " + param);
        }
        try {
            busIvy.sendMsg(robotName + " " + actionName + paramsString + " " + actionId);
        } catch (final IvyException ie) {
            System.out.println("can't send my valve state message on the bus");
        }
    }

    /**
     * Getter for name.
     *
     * @return The name of the IvyRxTx.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for bus.
     *
     * @return The Ivy bus (String).
     */
    public String getBus() {
        return this.bus;
    }

    /**
     * Getter for IvyBus.
     *
     * @return The Ivy bus (Ivy).
     */
    public Ivy getBusIvy() {
        return this.busIvy;
    }

    /**
     * Setter for Name.
     *
     * @param name Name of the IvyRxTx.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Stop the Ivy bus.
     */
    public void stop() {
        busIvy.stop();
    }
}
