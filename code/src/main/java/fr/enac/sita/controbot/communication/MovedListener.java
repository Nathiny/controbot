package fr.enac.sita.controbot.communication;

/**
 * MovedListener is used to receive the robot's position messages.
 */
@FunctionalInterface
public interface MovedListener {
    /**
     * Used for move message.
     *
     * @param robotName The robot's name.
     * @param x         The robot's x position.
     * @param y         The robot's y position.
     * @param theta     The robot's theta position.
     */
    void onMyEvent(String robotName, String x, String y, String theta);
}
