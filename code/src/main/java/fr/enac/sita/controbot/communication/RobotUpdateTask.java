package fr.enac.sita.controbot.communication;

import java.util.TimerTask;

import fr.enac.sita.controbot.model.robot.AbstractActuator;
import fr.enac.sita.controbot.model.robot.Robot;

/**
 * RobotUpdateTask extends TimerTask, is used to ask the current state of the
 * robot.
 */
public class RobotUpdateTask extends TimerTask {
    /** Robot. */
    private final Robot<AbstractActuator> robot;
    /** Ivy agent. */
    private final IvyRxTx ivy;

    /**
     * Start Timer for RobotUpdateTask.
     */
    @Override
    public void run() {
        ivy.getPos(robot.getName());
        ivy.getActuators(robot.getName(), robot.getActuators());
    }

    /**
     * Update the robot.
     *
     * @param robot The Robot.
     * @param ivy   The IvyRxTx.
     */
    public RobotUpdateTask(final Robot<AbstractActuator> robot, final IvyRxTx ivy) {
        this.robot = robot;
        this.ivy = ivy;
    }
}
