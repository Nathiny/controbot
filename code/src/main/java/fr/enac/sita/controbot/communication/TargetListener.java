package fr.enac.sita.controbot.communication;

/**
 * TargetListener is used to receive the robot's target messages.
 */
@FunctionalInterface
public interface TargetListener {
    /**
     * The method to launch on target event.
     *
     * @param robotName The robot's name.
     * @param x         The robot's x position.
     * @param y         The robot's y position.
     * @param tx        The target's x.
     * @param ty        The target's y.
     */
    void onMyTargetEvent(String robotName, String x, String y, String tx, String ty);
}
