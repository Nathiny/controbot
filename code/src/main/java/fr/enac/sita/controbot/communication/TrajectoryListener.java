package fr.enac.sita.controbot.communication;

/**
 * TrajectoryListener is used to receive the robot's trajectory messages.
 */
@FunctionalInterface
public interface TrajectoryListener {
    /**
     * The method to launch on trajectory events.
     *
     * @param robotName The robot's name.
     * @param finished  The state of the action (true for finished).
     */
    void onMyEvent(String robotName, String finished);
}
