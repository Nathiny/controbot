/**
 * Package managing the communication between controbot and the other Ivy
 * agents.
 */
package fr.enac.sita.controbot.communication;
