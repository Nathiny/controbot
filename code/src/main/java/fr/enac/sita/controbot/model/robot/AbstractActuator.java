package fr.enac.sita.controbot.model.robot;

/**
 * AbstractActuator is a model of an actuator.
 */
public abstract class AbstractActuator {

    /**
     * ID of the actuator.
     */
    private final String id;

    /**
     * Constructor for Actuator.
     *
     * @param id Id of the actuator.
     */
    public AbstractActuator(final String id) {
        this.id = id;
    }

    /**
     * Getter for id.
     *
     * @return The id of the actuator.
     */
    public String getId() {
        return id;
    }

}
