package fr.enac.sita.controbot.model.robot;

/**
 * Motor extends AbstractActuator, is a model of a motor.
 */
public class Motor extends AbstractActuator {

    /**
     * Rotation speed of the motor.
     */
    private int speed;

    /**
     * Direction of rotation of the motor (true if clockwise).
     */
    private Boolean clockwise;

    /**
     * Constructor for motor.
     *
     * @param id Id of the motor.
     */
    public Motor(final String id) {
        super(id);
        this.speed = 0;
        this.clockwise = true;
    }

    /**
     * Constructor for motor.
     *
     * @param id        Id of the motor. .
     * @param speed     Rotation speed of the motor.
     * @param clockwise Direction of rotation of the motor.
     */
    public Motor(final String id, final int speed, final Boolean clockwise) {
        super(id);
        this.speed = speed;
        this.clockwise = clockwise;
    }

    /**
     * Getter for speed.
     *
     * @return The speed of the motor.
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Setter for speed.
     *
     * @param speed Rotation speed of the motor.
     */
    public void setSpeed(final int speed) {
        this.speed = speed;
    }

    /**
     * Getter for clockwise.
     *
     * @return The direction of rotation of the motor.
     */
    public Boolean getClockwise() {
        return clockwise;
    }

    /**
     * Setter for clockwise.
     *
     * @param clockwise The direction of rotation of the motor.
     */
    public void setClockwise(final Boolean clockwise) {
        this.clockwise = clockwise;
    }

}
