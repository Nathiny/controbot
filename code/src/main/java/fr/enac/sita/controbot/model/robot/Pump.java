package fr.enac.sita.controbot.model.robot;

/**
 * Pump extends AbstractActuator, is a model of a pump.
 */
public class Pump extends AbstractActuator {

    /**
     * Power of the pump.
     */
    private int power;

    /**
     * Activity of the pump (true if something is cathched).
     */
    private Boolean sthCatched;

    /**
     * Constructor for pump.
     *
     * @param id Id of the pump.
     */
    public Pump(final String id) {
        super(id);
        this.power = 0;
        this.sthCatched = false;
    }

    /**
     * Constructor for pump.
     *
     * @param id         Id of the pump.
     * @param power      Power of the pump.
     * @param sthCatched Activity of the pump.
     */
    public Pump(final String id, final int power, final Boolean sthCatched) {
        super(id);
        this.power = power;
        this.sthCatched = sthCatched;
    }

    /**
     * Getter for power.
     *
     * @return The pump's power.
     */
    public int getPower() {
        return power;
    }

    /**
     * Setter for power.
     *
     * @param power The pump's power.
     */
    public void setPower(final int power) {
        this.power = power;
    }

    /**
     * Getter for sthCatched.
     *
     * @return If the pump catched something.
     */
    public Boolean getSthCatched() {
        return sthCatched;
    }

    /**
     * Setter for sthCatched.
     *
     * @param sthCatched If the pump catched something.
     */
    public void setSthCatched(final Boolean sthCatched) {
        this.sthCatched = sthCatched;
    }

}
