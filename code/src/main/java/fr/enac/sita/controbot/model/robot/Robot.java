package fr.enac.sita.controbot.model.robot;

import java.util.List;

/**
 * Robot is a model of a robot.
 *
 * @param <T> Actuator
 */
public class Robot<T> {
    /** Robot name. */
    private final String name;
    /** Robot x coordinates (center of rotation). */
    private int x;
    /** Robot y coordinates (center of rotation). */
    private int y;
    /** Robot theta coordinates (center of rotation). */
    private int theta;
    /** Robot battery percentage. */
    private final int battery;

    /** Robot color. */
    private String color;

    /** Robot width. */
    private final int width;

    /** Robot height. */
    private final int height;

    /** Robot speed. */
    private final int speed;

    /** Robot actuators. */
    private List<T> actuators;

    /** Robot startX. */
    private double startX;

    /** Robot startY. */
    private double startY;

    /** Robot endX. */
    private double endX;

    /** Robot endY. */
    private double endY;

    /**
     * Robot constructor.
     *
     * @param name    The robot's name.
     * @param x       The robot's x position.
     * @param y       The robot's y position.
     * @param battery The robot's battery.
     * @param width   The robot's width.
     * @param height  The robots height.
     * @param speed   The robot's speed.
     */
    public Robot(final String name, final int x, final int y, final int battery, final int width, final int height,
            final int speed) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.battery = battery;
        this.height = height;
        this.width = width;
        this.speed = speed;
    }

    /**
     * Robot constructor.
     *
     * @param name    The robot's name.
     * @param x       The robot's x position.
     * @param y       The robot's y position.
     * @param battery The robot's battery.
     * @param color   The robot's color.
     * @param width   The robot's width.
     * @param height  The robots height.
     * @param speed   The robot's speed.
     */
    public Robot(final String name, final int x, final int y, final int battery, final String color, final int width,
            final int height, final int speed) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.battery = battery;
        this.color = color;
        this.height = height;
        this.width = width;
        this.speed = speed;
    }

    /**
     * Getter for the name.
     *
     * @return Robot's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for the x position.
     *
     * @return Robot's x position.
     */
    public int getX() {
        return x;
    }

    /**
     * Setter for the x position.
     *
     * @param x Robot's x position.
     */
    public void setX(final int x) {
        this.x = x;
    }

    /**
     * Getter for the y position.
     *
     * @return Robot's y position.
     */
    public int getY() {
        return y;
    }

    /**
     * Setter for the y position.
     *
     * @param y Robot's y position.
     */
    public void setY(final int y) {
        this.y = y;
    }

    /**
     * Getter for the theta position.
     *
     * @return Robot's theta position.
     */
    public int getTheta() {
        return theta;
    }

    /**
     * Setter for the theta position.
     *
     * @param theta Robot's theta position.
     */
    public void setTheta(final int theta) {
        this.theta = theta;
    }

    /**
     * Getter for the battery percentage.
     *
     * @return Robot's battery.battery.
     */
    public int getBattery() {
        return battery;
    }

    /**
     * Getter for the color.
     *
     * @return Robot's color.
     */
    public String getColor() {
        return color;
    }

    /**
     * Getter for the width.
     *
     * @return Robot's width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Getter for the height.
     *
     * @return Robot's height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Getter for the speed.
     *
     * @return Robot's speed.
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Getter for the actuators list.
     *
     * @return Robot's actuator list.
     */
    public List<T> getActuators() {
        return actuators;
    }

    /**
     * Getter for the startX parameter.
     *
     * @return startX Trajectory's start x position.
     */
    public double getStartX() {
        return this.startX;
    }

    /**
     * Getter for the startY parameter.
     *
     * @return startY Trajectory's start y position.
     */
    public double getStartY() {
        return this.startY;
    }

    /**
     * Getter for the endX parameter.
     *
     * @return endX Trajectory's end x position.
     */
    public double getEndX() {
        return this.endX;
    }

    /**
     * Getter for the endY parameter.
     *
     * @return endY Trajectory's end y position.
     */
    public double getEndY() {
        return this.endY;
    }

    /**
     * Setter for the startX parameter.
     *
     * @param startX Trajectory's start x position.
     */
    public void setStartX(final double startX) {
        this.startX = startX;
    }

    /**
     * Setter for the startY parameter.
     *
     * @param startY Trajectory's start y position.
     */
    public void setStartY(final double startY) {
        this.startY = startY;
    }

    /**
     * Setter for the endX parameter.
     *
     * @param endX Trajectory's end x position.
     */
    public void setEndX(final double endX) {
        this.endX = endX;
    }

    /**
     * Setter for the endY parameter.
     *
     * @param endY Trajectory's end y position.
     */
    public void setEndY(final double endY) {
        this.endY = endY;
    }
}
