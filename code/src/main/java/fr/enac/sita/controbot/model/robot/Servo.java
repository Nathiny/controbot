package fr.enac.sita.controbot.model.robot;

/**
 * Servo extends AbstractActuator, is a model of a servo.
 */
public class Servo extends AbstractActuator {

    /**
     * Angle of the servo.
     */
    private int angle;

    /**
     * Constructor for servo.
     *
     * @param id    Servo's id.
     * @param angle Servo's angle.
     */
    public Servo(final String id, final int angle) {
        super(id);
        this.angle = angle;
    }

    /**
     * Constructor for servo.
     *
     * @param id Servo's id.
     */
    public Servo(final String id) {
        super(id);
    }

    /**
     * Getter for angle.
     *
     * @return Servo's angle.
     */
    public int getAngle() {
        return angle;
    }

    /**
     * Setter for angle.
     *
     * @param angle Servo's angle.
     */
    public void setAngle(final int angle) {
        this.angle = angle;
    }

}
