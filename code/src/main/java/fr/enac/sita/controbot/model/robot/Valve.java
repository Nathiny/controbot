package fr.enac.sita.controbot.model.robot;

/**
 * Valve extends AbstractActuator, is a model of a valve.
 */
public class Valve extends AbstractActuator {

    /**
     * State of the valve (true for open).
     */
    private Boolean open;

    /**
     * Constructor for valve.
     *
     * @param id   Valve's id.
     * @param open Valve's state.
     */
    public Valve(final String id, final Boolean open) {
        super(id);
        this.open = open;
    }

    /**
     * Getter for open.
     *
     * @return Valve's state (true for opened).
     */
    public Boolean getOpen() {
        return open;
    }

    /**
     * Setter for open.
     *
     * @param open Valve's state (true for opened).
     */
    public void setOpen(final Boolean open) {
        this.open = open;
    }

}
