/**
 * Package containing the Robot model and the actuators model.
 */
package fr.enac.sita.controbot.model.robot;
