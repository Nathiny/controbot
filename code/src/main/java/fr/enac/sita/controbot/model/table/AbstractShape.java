package fr.enac.sita.controbot.model.table;

/**
 * Shape used to represent an Obstacle.
 */
public abstract class AbstractShape {

    /**
     * Position x of the shape.
     */
    private double x;

    /**
     * Position y of the shape.
     */
    private double y;

    /**
     * Constructor for Shape.
     *
     * @param x Position x of the shape.
     * @param y Position y of the shape.
     */
    public AbstractShape(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Getter for x.
     *
     * @return x position of the shape.
     */
    public double getX() {
        return this.x;
    }

    /**
     * Setter for x.
     *
     * @param x x position of the shape.
     */
    public void setX(final double x) {
        this.x = x;
    }

    /**
     * Getter for y.
     *
     * @return y position of the shape.
     */
    public double getY() {
        return this.y;
    }

    /**
     * Setter for y.
     *
     * @param y y position of the shape.
     */
    public void setY(final double y) {
        this.y = y;
    }

}
