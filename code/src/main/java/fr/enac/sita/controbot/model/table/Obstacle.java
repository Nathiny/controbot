package fr.enac.sita.controbot.model.table;

/**
 * Obstacle represents an obstacle on the table.
 */
public class Obstacle {
    /** Obstacle name. */
    private String name;
    /** Obstacle shape. */
    private AbstractShape shape;

    /**
     * Constructor of an Obstacle.
     *
     * @param name  Obstacle's name.
     * @param shape Obstacle's AbstractShape.
     */
    public Obstacle(final String name, final AbstractShape shape) {
        this.name = name;
        this.shape = shape;
    }

    /**
     * Getter for obstacle's shape.
     *
     * @return Obstacle's shape.
     */
    public AbstractShape getShape() {
        return shape;
    }

    /**
     * Getter for obstacle's name.
     *
     * @return Obstacle's name.
     */
    public String getName() {
        return name;
    }
}
