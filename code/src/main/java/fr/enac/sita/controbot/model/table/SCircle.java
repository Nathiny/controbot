package fr.enac.sita.controbot.model.table;

/**
 * SCircle extends AbstractShape, is used to represent a circular Obstacle.
 */
public class SCircle extends AbstractShape {

    /**
     * Radius of the circle.
     */
    private final double radius;

    /**
     * Constructor for Circle.
     *
     * @param x      Circle's x position.
     * @param y      Circle's y position.
     * @param radius Circle's radius.
     */
    public SCircle(final double x, final double y, final double radius) {
        super(x, y);
        this.radius = radius;
    }

    /**
     * Getter for circle's radius.
     *
     * @return Circle's radius.
     */
    public double getRadius() {
        return radius;
    }
}
