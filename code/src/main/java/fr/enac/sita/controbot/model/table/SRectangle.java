package fr.enac.sita.controbot.model.table;

/**
 * SRectangle extends AbstractShape, is used to represent a rectangular
 * Obstacle.
 */
public class SRectangle extends AbstractShape {

    /**
     * Width of the rectangle.
     */
    private final double width;
    /**
     * Height of the rectangle.
     */
    private final double height;

    /**
     * Constructor for Rectangle.
     *
     * @param x      Position of the rectangle.
     * @param y      Position of the rectangle.
     * @param width  Width of the rectangle.
     * @param height Height of the rectangle.
     */
    public SRectangle(final double x, final double y, final double width, final double height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    /**
     * Getter for rectangle's width.
     *
     * @return Rectangle's width.
     */
    public double getWidth() {
        return width;
    }

    /**
     * Getter for rectangle's height.
     *
     * @return Rectangle's height.
     */
    public double getHeight() {
        return height;
    }

}
