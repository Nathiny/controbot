package fr.enac.sita.controbot.model.table;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;

import org.apache.commons.collections4.map.LinkedMap;

import fr.enac.sita.controbot.communication.IvyRxTx;
import fr.enac.sita.controbot.model.robot.AbstractActuator;
import fr.enac.sita.controbot.model.robot.Motor;
import fr.enac.sita.controbot.model.robot.Pump;
import fr.enac.sita.controbot.model.robot.Robot;
import fr.enac.sita.controbot.model.robot.Servo;
import fr.enac.sita.controbot.model.robot.Valve;
import javafx.scene.image.Image;

/**
 * Model of a table.
 */
public class Table {

    /** Name of the table. */
    private String name;
    /** Ground of the table. */
    private Image ground;
    /** List of obstacles. */
    private final Obstacle[] obs;
    /** Ivy bus. */
    private IvyRxTx ivy;
    /** List of the robots on the table. */
    private final LinkedMap<String, Robot<AbstractActuator>> robots = new LinkedMap<String, Robot<AbstractActuator>>();

    /** Width of the robot. */
    private final int width;

    /** Height of the robot. */
    private final int height;

    /**
     * Constructor for Table.
     *
     * @param name       Table's name.
     * @param groundFile Table's file's path containing the image.
     * @param obsFile    Table's file's path for the obstacles.
     * @param height     Table's height.
     * @param width      Table's width.
     * @throws IOException Exception for files.
     */
    public Table(final String name, final String groundFile, final String obsFile, final int height, final int width)
            throws IOException {
        this.name = name;
        final File groundImage = new File(groundFile);
        System.out.println("gdfile : " + groundFile);
        this.height = height;
        this.width = width;
        try {
            this.ground = new Image(groundImage.toURI().toURL().toExternalForm());
        } catch (final MalformedURLException e) {
            e.printStackTrace();
        }
        this.obs = obsInit(obsFile);
    }

    /**
     * Loads a robot's json.
     *
     * @param pathString Robot's json path.
     */
    public void loadRobot(final String pathString) {
        final Path path = Paths.get(pathString);
        String content;
        try {
            content = Files.readString(path);
            final RuntimeTypeAdapterFactory<AbstractActuator> runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory
                    .of(AbstractActuator.class, "type").registerSubtype(Servo.class, "servo")
                    .registerSubtype(Motor.class, "motor").registerSubtype(Pump.class, "pump")
                    .registerSubtype(Valve.class, "valve");
            final Gson gson = new GsonBuilder().registerTypeAdapterFactory(runtimeTypeAdapterFactory).create();
            final Type listType = new TypeToken<Robot<AbstractActuator>>() {
            }.getType();
            final Robot<AbstractActuator> robot = gson.fromJson(content, listType);
            addRobots(robot);
        } catch (final IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * @param robot Robot.
     */
    private void addRobots(final Robot<AbstractActuator> robot) {
        robots.put(robot.getName(), robot);
    }

    /**
     * Initialisation of Obstacle.
     *
     * @param file File's path containing the obstacles.
     * @return Obstacle list.
     * @throws IOException Exception for files.
     */
    public Obstacle[] obsInit(final String file) throws IOException {
        int counter = 0;
        final BufferedReader buff = new BufferedReader(new FileReader(file));
        while (buff.readLine() != null) {
            counter++;
        }
        buff.close();
        final Obstacle[] obstacles = new Obstacle[counter];
        int i = 0;

        final BufferedReader in = new BufferedReader(new FileReader(file));
        String line;
        while ((line = in.readLine()) != null) {
            final String[] l = line.split(";");
            final String nameObs = l[0];
            if (l[1].equals("circle")) {
                final double x = Double.parseDouble(l[2]);
                final double y = Double.parseDouble(l[3]);
                final double radius = Double.parseDouble(l[4]);
                final SCircle circle = new SCircle(x, y, radius);
                obstacles[i] = new Obstacle(nameObs, circle);
            }
            if (l[1].equals("rectangle")) {
                final double x = Double.parseDouble(l[2]);
                final double y = Double.parseDouble(l[3]);
                final double widthObs = Double.parseDouble(l[4]);
                final double heightObs = Double.parseDouble(l[5]);
                final SRectangle rectangle = new SRectangle(x, y, widthObs, heightObs);
                obstacles[i] = new Obstacle(nameObs, rectangle);
            }
            i++;
        }
        in.close();
        return obstacles;
    }

    /**
     * Getter for Name.
     *
     * @return Table's name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for Ground.
     *
     * @return Ground's image.
     */
    public Image getGround() {
        return this.ground;
    }

    /**
     * Getter for Obs.
     *
     * @return Obstacle list.
     */
    public Obstacle[] getObs() {
        return this.obs;
    }

    /**
     * Setter for Name.
     *
     * @param name Table's name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Getter for IvyRxTx.
     *
     * @return The IvyRxTx.
     */
    public IvyRxTx getIvy() {
        return ivy;
    }

    /**
     * Setter for IvyRxTx.
     *
     * @param ivy The IvyRxTx.
     */
    public void setIvy(final IvyRxTx ivy) {
        this.ivy = ivy;

    }

    /**
     * Getter for robot list.
     *
     * @return Robot list.
     */
    public LinkedMap<String, Robot<AbstractActuator>> getRobots() {
        return robots;
    }

    /**
     * Getter for table's width.
     *
     * @return Table's width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Getter for table's height.
     *
     * @return Table's height.
     */
    public int getHeight() {
        return height;
    }

}
