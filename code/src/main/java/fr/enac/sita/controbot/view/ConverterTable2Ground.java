package fr.enac.sita.controbot.view;

import fr.enac.sita.controbot.model.table.Table;
import javafx.scene.image.ImageView;

/**
 * A ConverterTable2Ground is used to make conversions between the table frame
 * and the ground ImageView frame.
 */
public class ConverterTable2Ground {
    /** OffsetX in the view. */
    private double offsetX;
    /** OffsetY in the view. */
    private double offsetY;
    /** Height of the ground (image view). */
    private double groundHeight;
    /** Width of the ground (image view). */
    private double groundWidth;
    /** Ratio width / height of the ground (image view). */
    private double ratioGround;
    /** Ratio width / height of the table (model). */
    private double ratioTable;
    /** Coefficient table to ground for width. */
    private double kWidthTable2Ground;
    /** Coefficient table to ground for height. */
    private double kHeightTable2Ground;

    /**
     * Converts.
     *
     * @param table  Table.
     * @param ground Image of the Table.
     */
    public void convert(final Table table, final ImageView ground) {
        ratioGround = ground.getFitWidth() / ground.getFitHeight();
        ratioTable = Double.valueOf(table.getWidth()) / Double.valueOf(table.getHeight());
        if (ratioTable > ratioGround) {
            groundHeight = ground.getFitWidth() / ratioTable;
            groundWidth = ground.getFitWidth();
            offsetY = (ground.getFitHeight() - groundHeight) / 2;
            offsetX = 0;
        } else {
            groundHeight = ground.getFitHeight();
            groundWidth = ground.getFitHeight() * ratioTable;
            offsetX = (ground.getFitWidth() - groundWidth) / 2;
            offsetY = 0;
        }
        kWidthTable2Ground = groundWidth / Double.valueOf(table.getWidth());
        kHeightTable2Ground = groundHeight / Double.valueOf(table.getHeight());
    }

    /**
     * Convert width from table (model) to ground (image view).
     *
     * @param value Value.
     * @return The converted value.
     */
    public double table2GroundWidth(final double value) {
        return value * kWidthTable2Ground;
    }

    /**
     * Convert height from table (model) to ground (image view).
     *
     * @param value Value.
     * @return The converted value.
     */
    public double table2GroundHeight(final double value) {
        return value * kHeightTable2Ground;
    }

    /**
     * Convert width from ground (image view) to table (model).
     *
     * @param value Value.
     * @return The converted value.
     */
    public double ground2TableWidth(final double value) {
        return value / kWidthTable2Ground;
    }

    /**
     * Convert height from ground (image view) to table (model).
     *
     * @param value Value.
     * @return The converted value.
     */
    public double ground2TableHeight(final double value) {
        return value / kHeightTable2Ground;
    }

    /**
     * Getter for the x offset.
     *
     * @return Offset for the table x position.
     */
    public double getOffsetX() {
        return offsetX;
    }

    /**
     * Getter for the y offset.
     *
     * @return Offset for the table y position.
     */
    public double getOffsetY() {
        return offsetY;
    }

}
