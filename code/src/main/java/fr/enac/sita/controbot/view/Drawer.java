package fr.enac.sita.controbot.view;

import java.util.ArrayList;
import java.util.Hashtable;

import fr.enac.sita.controbot.model.table.Table;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeType;
import fr.enac.sita.controbot.model.table.Obstacle;
import fr.enac.sita.controbot.model.table.SRectangle;
import fr.enac.sita.controbot.model.table.SCircle;

/**
 * A Drawer is used to draw the objects on the map.
 */
public final class Drawer {

    /**
     * No constructor for Utility classes.
     */
    private Drawer() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Initializes the shapes of the obstacles.
     *
     * @param obs    Obstacle array.
     * @param pane   Global Pane.
     * @param table  Table.
     * @param ground Image of the table.
     * @return Obstacle's shapes.
     */
    public static ArrayList<Shape> drawObstaclesInit(final Obstacle[] obs, final Pane pane, final Table table,
            final ImageView ground) {
        ArrayList<Shape> shapes = new ArrayList<Shape>();
        ConverterTable2Ground ct2g = new ConverterTable2Ground();
        ct2g.convert(table, ground);
        for (int i = 0; i < obs.length; i++) {
            double x = ct2g.table2GroundWidth(obs[i].getShape().getX()) + ct2g.getOffsetX();
            double y = ct2g.table2GroundHeight(obs[i].getShape().getY()) + ct2g.getOffsetY();
            if (obs[i].getShape() instanceof SCircle) {
                SCircle c = (SCircle) obs[i].getShape();
                double radius = ct2g.table2GroundHeight(c.getRadius());
                Circle circle = new Circle(x, y, radius);
                circle.setStroke(Color.DARKGREY);
                circle.setStrokeType(StrokeType.INSIDE);
                pane.getChildren().add(circle);
                shapes.add((Shape) circle);
            }
            if (obs[i].getShape() instanceof SRectangle) {
                SRectangle r = (SRectangle) obs[i].getShape();
                double width = ct2g.table2GroundWidth(r.getWidth());
                double height = ct2g.table2GroundHeight(r.getHeight());
                Rectangle rectangle = new Rectangle(x, y, width, height);
                pane.getChildren().add(rectangle);
                shapes.add((Shape) rectangle);
            }
        }
        return shapes;
    }

    /**
     * Draws the obstacles.
     *
     * @param obs    Obstacle array.
     * @param shapes Shape array.
     * @param table  Table.
     * @param ground Image of the Table.
     */
    public static void drawObstacles(final Obstacle[] obs, final ArrayList<Shape> shapes, final Table table,
            final ImageView ground) {
        ConverterTable2Ground ct2g = new ConverterTable2Ground();
        ct2g.convert(table, ground);
        for (int i = 0; i < obs.length; i++) {
            double x = ct2g.table2GroundWidth(obs[i].getShape().getX()) + ct2g.getOffsetX();
            double y = ct2g.table2GroundHeight(obs[i].getShape().getY()) + ct2g.getOffsetY();
            if (obs[i].getShape() instanceof SCircle) {
                SCircle c = (SCircle) obs[i].getShape();
                Circle circle = (Circle) shapes.get(i);
                circle.setCenterX(x);
                circle.setCenterY(y);
                circle.setRadius(ct2g.table2GroundHeight(c.getRadius()));
            }
            if (obs[i].getShape() instanceof SRectangle) {
                SRectangle r = (SRectangle) obs[i].getShape();
                Rectangle rectangle = (Rectangle) shapes.get(i);
                rectangle.setX(x);
                rectangle.setY(y);
                rectangle.setHeight(ct2g.table2GroundHeight(r.getHeight()));
                rectangle.setWidth(ct2g.table2GroundWidth(r.getWidth()));
            }
        }
    }

    /**
     * Draws the robot.
     *
     * @param robotName  Name of the robot.
     * @param robotsView Shapes of the robots.
     * @param table      Table.
     * @param ground     Image of the Table.
     */
    public static void drawRobot(final String robotName, final Hashtable<String, Shape> robotsView, final Table table,
            final ImageView ground) {
        final int robotX = table.getRobots().get(robotName).getX();
        final int robotY = table.getRobots().get(robotName).getY();
        final int robotWidth = table.getRobots().get(robotName).getWidth();
        final int robotHeight = table.getRobots().get(robotName).getHeight();

        final double viewX;
        final double viewY;
        final int viewWidth;
        final int viewHeight;

        ConverterTable2Ground ct2g = new ConverterTable2Ground();
        ct2g.convert(table, ground);

        Rectangle rect = (Rectangle) robotsView.get(robotName);

        viewHeight = (int) ct2g.table2GroundHeight(robotHeight);
        rect.setHeight(viewHeight);

        viewWidth = (int) ct2g.table2GroundWidth(robotWidth);
        rect.setWidth(viewWidth);

        viewX = ct2g.getOffsetX() + ct2g.table2GroundWidth(robotX) - rect.getWidth() / 2;
        rect.setLayoutX(viewX);

        viewY = ct2g.getOffsetY() + ct2g.table2GroundHeight(robotY) - rect.getHeight() / 2;
        rect.setLayoutY(viewY);

        final int rightAngle = 90;
        rect.setRotate(table.getRobots().get(robotName).getTheta() + rightAngle);
    }

    /**
     * Draws the trajectory.
     *
     * @param table      Table.
     * @param robotName  Name of the robot.
     * @param trajectory Trajectory to draw.
     * @param ground     Image of the table.
     */
    public static void drawTrajectory(final Table table, final String robotName, final Line trajectory,
            final ImageView ground) {
        ConverterTable2Ground ct2g = new ConverterTable2Ground();
        ct2g.convert(table, ground);

        trajectory.setStartX(ct2g.table2GroundWidth(table.getRobots().get(robotName).getStartX()) + ct2g.getOffsetX());
        trajectory.setStartY(ct2g.table2GroundHeight(table.getRobots().get(robotName).getStartY()) + ct2g.getOffsetY());
        trajectory.setEndX(ct2g.table2GroundWidth(table.getRobots().get(robotName).getEndX()) + ct2g.getOffsetX());
        trajectory.setEndY(ct2g.table2GroundHeight(table.getRobots().get(robotName).getEndY()) + ct2g.getOffsetY());
    }
}
