package fr.enac.sita.controbot.view;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fr.enac.sita.controbot.actions.ActionsSequence;
import fr.enac.sita.controbot.communication.RobotUpdateTask;
import fr.enac.sita.controbot.model.table.Obstacle;
import fr.enac.sita.controbot.model.table.Table;
import fr.enac.sita.controbot.model.robot.AbstractActuator;
import fr.enac.sita.controbot.model.robot.Motor;
import fr.enac.sita.controbot.model.robot.Pump;
import fr.enac.sita.controbot.model.robot.Robot;
import fr.enac.sita.controbot.model.robot.Servo;
import fr.enac.sita.controbot.model.robot.Valve;
import fr.enac.sita.controbot.view.controllers.MotorController;
import fr.enac.sita.controbot.view.controllers.PositionController;
import fr.enac.sita.controbot.view.controllers.PumpController;
import fr.enac.sita.controbot.view.controllers.ServoController;
import fr.enac.sita.controbot.view.controllers.ValveController;
import javafx.geometry.Insets;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * A Loader is used to load robots, tables or configurations.
 */
public final class Loader {

    /**
     * Spacing size.
     */
    static final int SPACING = 5;

    /**
     * No constructor for Utility classes.
     */
    private Loader() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Loads the robot.
     *
     * @param pathRobot         Path to the robot json file.
     * @param movableObjectPane Pane in which are the robots.
     * @param actuatorsView     View of the actuators.
     * @param robotsView        Hashtable of the robots shape.
     * @param view              Global View.
     * @param table             Table.
     * @param ground            Image of the table.
     * @param linesView         Hashtable of the lines.
     */
    public static void loadRobot(final Path pathRobot, final Pane movableObjectPane, final Accordion actuatorsView,
            final Hashtable<String, Shape> robotsView, final View view, final Table table, final ImageView ground,
            final Hashtable<String, Line> linesView) {
        try {
            final int refreshRATE = 200;
            table.loadRobot(pathRobot.toString());
            initDrawRobot(movableObjectPane, table.getRobots().get(table.getRobots().lastKey()), actuatorsView,
                    robotsView, view, table, ground, linesView);
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(
                    new RobotUpdateTask(table.getRobots().get(table.getRobots().lastKey()), table.getIvy()),
                    refreshRATE, refreshRATE);
            ground.fitHeightProperty().addListener(ev -> table.getRobots()
                    .forEach((robotName, robot) -> Drawer.drawRobot(robotName, robotsView, table, ground)));
            ground.fitWidthProperty().addListener(ev -> table.getRobots()
                    .forEach((robotName, robot) -> Drawer.drawRobot(robotName, robotsView, table, ground)));
        } catch (Exception e1) {
            e1.printStackTrace();
            String s = "No valid robot selected.";
            System.out.println(s);
            view.alertError(s);
        }

    }

    /**
     * Initializes the rectangle of the robot.
     *
     * @param pane          Pane of the robots.
     * @param robot         Robot to initialize.
     * @param actuatorsView Accordion of the actuators.
     * @param robotsView    Hashtable of the robots shape.
     * @param view          Global view.
     * @param table         Table.
     * @param ground        Image of the table.
     * @param linesView     Hashtable of the lines.
     */
    public static void initDrawRobot(final Pane pane, final Robot<AbstractActuator> robot,
            final Accordion actuatorsView, final Hashtable<String, Shape> robotsView, final View view,
            final Table table, final ImageView ground, final Hashtable<String, Line> linesView) {
        final int actuatorWIDTH = 230;
        Rectangle rect = new Rectangle(robot.getWidth(), robot.getHeight());
        Label label = new Label(robot.getName());
        label.setStyle("-fx-background-color: skyblue; -fx-font-size: 20; -fx-text-fill: black;");
        // label.setFont(new Font("Arial", 20));
        // label.setBackground(new Background(new BackgroundFill(Color.WHITE,
        // CornerRadii.EMPTY, Insets.EMPTY)));
        label.setVisible(false);
        label.layoutXProperty().bind(rect.layoutXProperty());
        label.layoutYProperty().bind(rect.layoutYProperty());
        rect.setOnMouseEntered((MouseEvent e) -> {
            label.setVisible(true);
        });
        rect.setOnMouseExited((MouseEvent e) -> {
            label.setVisible(false);
        });
        final Color color = Color.valueOf(robot.getColor());
        rect.setStroke(color);
        rect.setStrokeType(StrokeType.INSIDE);
        // rect.setFill(color.deriveColor(1, 1, 1, 1));
        rect.setStyle("-fx-fill: linear-gradient(to top, " + robot.getColor() + " 85%, black 15%);");

        robotsView.put(robot.getName(), rect);
        Line traj = new Line();
        linesView.put(robot.getName(), traj);
        pane.getChildren().addAll(rect, label);
        pane.getChildren().add(traj);
        // drawRobot(robot.getName());
        final String robotName = robot.getName();
        Drawer.drawRobot(robotName, robotsView, table, ground);
        VBox actuatorsContent = new VBox();
        addActuators(robot, actuatorsContent, view);
        ScrollPane actuatorsScroll = new ScrollPane(actuatorsContent);
        actuatorsScroll.setPrefWidth(actuatorWIDTH);
        TitledPane robotTitle = new TitledPane(robot.getName(), actuatorsScroll);
        actuatorsView.getPanes().add(robotTitle);
    }

    /**
     * Adds the content to the title pane.
     *
     * @param robot   Robot.
     * @param content VBox.
     * @param view    Global View.
     */
    private static void addActuators(final Robot<AbstractActuator> robot, final VBox content, final View view) {
        final int paddingTOP = 10;
        final int paddingRIGHT = 0;
        final int paddingBOTTOM = 10;
        final int paddingLEFT = 0;
        HBox battery = new HBox();
        Label batteryTitle = new Label("Battery (%): ");
        Label batteryState = new Label(Integer.toString(robot.getBattery()));
        battery.getChildren().addAll(batteryTitle, batteryState);
        battery.setPadding(new Insets(paddingTOP, paddingRIGHT, paddingBOTTOM, paddingLEFT));

        VBox sequencesContent = new VBox();
        TitledPane sequences = new TitledPane("Sequences", sequencesContent);
        sequencesContent.setSpacing(SPACING);

        VBox positionContent = new VBox();
        TitledPane position = new TitledPane("Position", positionContent);
        positionContent.setSpacing(SPACING);
        positionContent.setPadding(new Insets(SPACING, SPACING, SPACING, SPACING));
        PositionController posc = new PositionController(robot, view, positionContent);
        posc.update();
        content.getChildren().addAll(battery, sequences, position);

        VBox servosContent = new VBox();
        TitledPane servos = new TitledPane("Servos", servosContent);
        servosContent.setSpacing(SPACING);
        servosContent.setPadding(new Insets(SPACING, SPACING, SPACING, SPACING));
        servos.setExpanded(false);

        VBox motorsContent = new VBox();
        TitledPane motors = new TitledPane("Motors", motorsContent);
        motorsContent.setSpacing(SPACING);
        motorsContent.setPadding(new Insets(SPACING, SPACING, SPACING, SPACING));
        motors.setExpanded(false);

        VBox pumpsContent = new VBox();
        TitledPane pumps = new TitledPane("Pumps", pumpsContent);
        pumpsContent.setSpacing(SPACING);
        pumpsContent.setPadding(new Insets(SPACING, SPACING, SPACING, SPACING));
        pumps.setExpanded(false);

        VBox valvesContent = new VBox();
        TitledPane valves = new TitledPane("Valves", valvesContent);
        valvesContent.setSpacing(SPACING);
        valvesContent.setPadding(new Insets(SPACING, SPACING, SPACING, SPACING));
        valves.setExpanded(false);

        List<AbstractActuator> actuators = robot.getActuators();

        ListIterator<AbstractActuator> it = actuators.listIterator();
        while (it.hasNext()) {
            AbstractActuator actuator = it.next();
            if (actuator instanceof Servo) {
                ServoController sc = new ServoController((Servo) actuator, view, servosContent, robot.getName());
                sc.update(); // for controller screen update
                if (!content.getChildren().contains(servos)) {
                    content.getChildren().add(servos);
                }
            } else if (actuator instanceof Motor) {
                MotorController mc = new MotorController((Motor) actuator, view, motorsContent, robot.getName());
                mc.update();
                if (!content.getChildren().contains(motors)) {
                    content.getChildren().add(motors);
                }
            } else if (actuator instanceof Pump) {
                PumpController pc = new PumpController((Pump) actuator, view, pumpsContent, robot.getName());
                pc.update();
                if (!content.getChildren().contains(pumps)) {
                    content.getChildren().add(pumps);
                }
            } else if (actuator instanceof Valve) {
                ValveController vc = new ValveController((Valve) actuator, view, valvesContent, robot.getName());
                vc.update();
                if (!content.getChildren().contains(valves)) {
                    content.getChildren().add(valves);
                }
            }
        }
    }

    /**
     * Loads a sequence.
     *
     * @param pathSequence  Path to the sequence json file.
     * @param table         Table.
     * @param actuatorsView Accordion of the actuators.
     * @param view          Global view.
     */
    public static void loadSequence(final Path pathSequence, final Table table, final Accordion actuatorsView,
            final View view) {
        JsonParser parserSequence = new JsonParser();
        String contentSequence;
        try {
            contentSequence = Files.readString(pathSequence);
            JsonObject jsonObjectSequence = parserSequence.parse(contentSequence).getAsJsonObject();
            String idAction = view.jsonGetString(jsonObjectSequence, "id");
            String actionsJson = View.jsonGetArray(jsonObjectSequence, "actions");
            // System.out.println(actionsJson);
            String robotName = view.jsonGetString(jsonObjectSequence, "robot");
            if (table.getRobots().get(robotName) != null) {
                ActionsSequence sequence = new ActionsSequence(idAction, actionsJson, table.getIvy(), robotName);
                Button btnSequence = new Button("Replay " + idAction);
                btnSequence.setOnAction(ev -> {
                    new Thread(sequence).start();
                });
                for (TitledPane tp : actuatorsView.getPanes()) {
                    if (tp.getText().equals(robotName)) {
                        ((VBox) ((TitledPane) ((VBox) ((ScrollPane) tp.getContent()).getContent()).getChildren().get(1))
                                .getContent()).getChildren().add(btnSequence);
                    }
                }
            } else {
                view.alertError(robotName + " is not loaded.");
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            String s = "No valid sequence selected.";
            System.out.println(s);
            view.alertError(s);
        }
    }

    /**
     * Loads a configuration file.
     *
     * @param fileChooser       FileChooser.
     * @param primaryStage      Global Stage.
     * @param movableObjectPane Robots Pane.
     * @param actuatorsView     Accordion of the actuators.
     * @param pane              Global Pane.
     * @param view              Global view.
     * @param robotsView        Shapes of the robots.
     * @param ground            Table Image.
     * @param linesView         Hashtables of the lines.
     */
    public static void loadConfig(final FileChooser fileChooser, final Stage primaryStage, final Pane movableObjectPane,
            final Accordion actuatorsView, final StackPane pane, final View view,
            final Hashtable<String, Shape> robotsView, final ImageView ground,
            final Hashtable<String, Line> linesView) {
        File jsonFile = fileChooser.showOpenDialog(primaryStage);
        Path path = Paths.get(jsonFile.getPath());
        JsonParser parser = new JsonParser();
        String content;
        try {
            // loading table
            content = Files.readString(path);
            JsonObject jsonObject = parser.parse(content).getAsJsonObject();
            String pathTable = View.jsonGetDirectory(jsonFile, jsonObject, "table");
            loadTable(Paths.get(pathTable), movableObjectPane, actuatorsView, view, ground, robotsView);
            String ivyBus = view.jsonGetString(jsonObject, "ivyBus");
            if (view.getTable() != null) {
                // Connection
                view.keyConnect(actuatorsView);
                view.mouseConnect(actuatorsView, pane);
                view.ivyConnect(ivyBus);
                if (view.getTable().getIvy() != null) {
                    Gson gson = new Gson();
                    String rootPath = jsonFile.getAbsolutePath().toString().substring(0,
                            jsonFile.getAbsolutePath().toString().length() - jsonFile.getName().length());
                    // loading robot
                    try {
                        String robotPathsJson = jsonObject.get("robots").toString();
                        String[] robotPaths = gson.fromJson(robotPathsJson, String[].class);
                        for (String pathRobot : robotPaths) {
                            String finalPathRobot = rootPath + pathRobot.replaceAll("^\"|\"$", "");
                            // loadRobot(Paths.get(finalPathRobot), movableObjectPane, actuatorsView);
                            loadRobot(Paths.get(finalPathRobot), movableObjectPane, actuatorsView, robotsView, view,
                                    view.getTable(), ground, linesView);
                        }
                    } catch (Exception e) {
                        System.out.println("No robot or something wrong with loading robot happens");
                        // e.printStackTrace();
                    }
                    // Loading sequence
                    try {
                        String sequencePathsJson = jsonObject.get("sequences").toString();
                        String[] sequencePaths = gson.fromJson(sequencePathsJson, String[].class);
                        for (String pathSequence : sequencePaths) {
                            String finalPathSequence = rootPath + pathSequence.replaceAll("^\"|\"$", "");
                            loadSequence(Paths.get(finalPathSequence), view.getTable(), actuatorsView, view);
                        }
                    } catch (Exception e) {
                        System.out.println("No sequence or something wrong with loading sequence happens");
                        // e.printStackTrace();
                    }

                } else {
                    view.alertError("No ivy bus found!");
                }
            } else {
                view.alertError("No table found!");
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            String s = "No valid config selected.";
            System.out.println(s);
            view.alertError(s);
        }
    }

    /**
     * Loads a table.
     *
     * @param pathTable         Path to the table's json file
     * @param movableObjectPane Pane of the robots.
     * @param actuatorsView     Accordion of the actuators.
     * @param view              Global View.
     * @param ground            Image of the Table.
     * @param robotsView        Shapes of the robots.
     */
    public static void loadTable(final Path pathTable, final Pane movableObjectPane, final Accordion actuatorsView,
            final View view, final ImageView ground, final Hashtable<String, Shape> robotsView) {
        JsonParser parser = new JsonParser();
        try {
            File file = new File(pathTable.toString());
            String content = Files.readString(pathTable);
            JsonObject jsonObject = parser.parse(content).getAsJsonObject();
            String pathImage = View.jsonGetDirectory(file, jsonObject, "ground");
            String pathOsbtacles = View.jsonGetDirectory(file, jsonObject, "obstacles");
            String name = view.jsonGetString(jsonObject, "name");
            int width = Integer.parseInt(view.jsonGetString(jsonObject, "width"));
            int height = Integer.parseInt(view.jsonGetString(jsonObject, "height"));
            actuatorsView.getPanes().clear();
            movableObjectPane.getChildren().clear();
            try {
                view.getTable().getIvy().stop();
            } catch (Exception exc) {
                System.out.println("No Ivy");
            }

            Table table = new Table(name, pathImage, pathOsbtacles, height, width);
            view.setTable(table);
            Obstacle[] obs = table.getObs();

            ArrayList<Shape> shapes = Drawer.drawObstaclesInit(obs, movableObjectPane, table, ground);
            ground.fitHeightProperty().addListener(ev -> Drawer.drawObstacles(obs, shapes, view.getTable(), ground));
            ground.fitWidthProperty().addListener(ev -> Drawer.drawObstacles(obs, shapes, view.getTable(), ground));
        } catch (Exception e1) {
            String s = "No valid table selected.";
            System.out.println(s);
            view.alertError(s);
            e1.printStackTrace();
        }

        robotsView.clear();
        ground.setImage(view.getTable().getGround());
        ground.setPreserveRatio(true);
    }

}
