package fr.enac.sita.controbot.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Optional;

import com.google.gson.JsonObject;

import fr.enac.sita.controbot.communication.IvyRxTx;
import fr.enac.sita.controbot.model.table.Table;
import fr.enac.sita.controbot.model.robot.AbstractActuator;
import fr.enac.sita.controbot.model.robot.Robot;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * View extends Application, is used to show the table and the control panel.
 */
public class View extends Application {

    /** Name of the view. */
    private String name;
    /** Scene of the view. */
    private Scene scene;
    /** Table. */
    private Table table;
    /** Image of the ground. */
    private final ImageView ground;

    /** Hashtable containing rectangles of robots with their name. */
    private Hashtable<String, Shape> robotsView = new Hashtable<String, Shape>();

    /** Hashtable containing trajectories of robots with their name. */
    private Hashtable<String, Line> linesView = new Hashtable<String, Line>();

    /**
     * Constructor for View.
     *
     * @throws java.io.IOException File's exception.
     */
    public View() throws IOException {
        this.name = "Controbot View";
        this.ground = new ImageView();

    }

    /**
     * Getter for View's name.
     *
     * @return View's name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for view's scene.
     *
     * @return View's scene
     */
    public Scene getScene() {
        return this.scene;
    }

    /**
     * Setter for View's name.
     *
     * @param name View's name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    private void close() {
        Platform.exit();
        System.exit(0);
    }

    /**
     * Used by main to start the view.
     */
    @Override
    public void start(final Stage primaryStage) throws Exception {
        final int initHEIGHT = 600;
        final double initRATIO = 1.6;

        primaryStage.setOnCloseRequest(e -> {
            close();
        });

        BorderPane root = new BorderPane();
        StackPane pane = new StackPane();
        StackPane immovableObjectPane = new StackPane();
        Pane movableObjectPane = new Pane();

        Accordion actuatorsView = new Accordion();
        root.setRight(actuatorsView);

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter fileExtensions = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");

        fileChooser.getExtensionFilters().add(fileExtensions);
        fileChooser.setTitle("Open Resource File");

        MenuBar menuBar = new MenuBar();
        root.setTop(menuBar);
        Menu fileMenu = new Menu("File");
        MenuItem configItem = new MenuItem("Load config");
        MenuItem tableItem = new MenuItem("Load table");
        MenuItem robotItem = new MenuItem("Load robot");
        MenuItem sequenceItem = new MenuItem("Load sequence");
        Menu ivyMenu = new Menu("Ivy");
        MenuItem ivyChooserItem = new MenuItem("Select ivy bus");
        SeparatorMenuItem separatorConfig = new SeparatorMenuItem();
        SeparatorMenuItem separatorExit = new SeparatorMenuItem();

        MenuItem exitItem = new MenuItem("Exit");

        configItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN));
        tableItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN));
        robotItem.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN));
        ivyChooserItem.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN));
        sequenceItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        exitItem.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));

        ivyMenu.getItems().add(ivyChooserItem);
        fileMenu.getItems().add(configItem);
        fileMenu.getItems().add(separatorConfig);
        fileMenu.getItems().add(tableItem);
        fileMenu.getItems().add(robotItem);
        fileMenu.getItems().add(sequenceItem);
        fileMenu.getItems().add(separatorExit);
        fileMenu.getItems().add(exitItem);

        menuBar.getMenus().add(fileMenu);
        menuBar.getMenus().add(ivyMenu);

        exitItem.setOnAction(e -> close());

        ivyChooserItem.setOnAction(e -> {
            if (table != null) {
                TextInputDialog dialog = new TextInputDialog("192.168.1.0:25565");
                try {
                    table.getIvy().stop();
                } catch (Exception exc) {
                    System.out.println("No Ivy");
                }
                dialog.setTitle("Ivy bus");
                dialog.setHeaderText("Ivy bus");
                dialog.setContentText("Please enter the ivy bus:");
                dialog.initOwner(this.getScene().getWindow());
                dialog.initModality(Modality.APPLICATION_MODAL);
                Optional<String> result = dialog.showAndWait();
                result.ifPresent(ivyBus -> {
                    final String ipRegex = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b:\\d{1,5}$";
                    if (ivyBus.matches(ipRegex)) {
                        keyConnect(actuatorsView);
                        mouseConnect(actuatorsView, pane);
                        ivyConnect(ivyBus);
                    } else {
                        alertError("Invalid Ivy bus!");
                    }
                });
            } else {
                alertError("Looks like there is no table!");
            }
        });

        robotItem.setOnAction(e -> {
            if (table != null && table.getIvy() != null) {
                File jsonfile = fileChooser.showOpenDialog(primaryStage);
                Path robotPath = Paths.get(jsonfile.getPath());
                // loadRobot(robotPath, movableObjectPane, actuatorsView);
                Loader.loadRobot(robotPath, movableObjectPane, actuatorsView, robotsView, this, table, ground,
                        linesView);
            } else {
                alertError("Looks like there is no table or ivy bus selected!");
            }
        });

        sequenceItem.setOnAction(e -> {
            if (table != null && table.getIvy() != null) {
                File fileSequence = fileChooser.showOpenDialog(primaryStage);
                Path pathSequence = Paths.get(fileSequence.getPath());
                Loader.loadSequence(pathSequence, table, actuatorsView, this);
            } else {
                alertError("Looks like there is no table or ivy bus selected!");
            }
        });

        tableItem.setOnAction(e -> {
            File jsonFile = fileChooser.showOpenDialog(primaryStage);
            Path path = Paths.get(jsonFile.getPath());
            Loader.loadTable(path, movableObjectPane, actuatorsView, this, ground, robotsView);
        });
        configItem.setOnAction(e -> {
            Loader.loadConfig(fileChooser, primaryStage, movableObjectPane, actuatorsView, pane, this, robotsView,
                    ground, linesView);

        });

        root.setCenter(pane);
        pane.getChildren().add(immovableObjectPane);
        pane.getChildren().add(movableObjectPane);

        immovableObjectPane.getChildren().add(ground);

        scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.setWidth(initRATIO * initHEIGHT);
        primaryStage.setHeight(initHEIGHT);
        primaryStage.show();

        ground.fitWidthProperty().bind(immovableObjectPane.widthProperty());
        ground.fitHeightProperty().bind(immovableObjectPane.heightProperty());

        immovableObjectPane.setMinSize(0, 0);
        movableObjectPane.setMinSize(0, 0);
    }

    /**
     * Displays the message in an error window.
     *
     * @param message Message to display in error window.
     */
    public void alertError(final String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText("There was an error");
        alert.setContentText(message);

        alert.initOwner(this.getScene().getWindow());
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.showAndWait();
    }

    /**
     * Setter for the table.
     *
     * @param table View's table to set.
     */
    public void setTable(final Table table) {
        this.table = table;
    }

    /**
     * Returns the directory of the file.
     *
     * @param jsonFile   Json file to read.
     * @param jsonObject Json object to read.
     * @param key        Key to check.
     * @return Path's directory of the file.
     */
    public static String jsonGetDirectory(final File jsonFile, final JsonObject jsonObject, final String key) {
        return jsonFile.getAbsolutePath().toString().substring(0,
                jsonFile.getAbsolutePath().toString().length() - jsonFile.getName().length())
                + jsonObject.get(key).toString().replaceAll("^\"|\"$", "");
    }

    /**
     * Returns the string of the jsonObject in key element.
     *
     * @param jsonObject to read.
     * @param key        to check.
     * @return Json file's string
     */
    public String jsonGetString(final JsonObject jsonObject, final String key) {
        String s = "";
        try {
            s = jsonObject.get(key).toString().replaceAll("^\"|\"$", "");
        } catch (Exception e) {
            alertError(e.toString() + ": no \"" + key + "\" found in :\n" + jsonObject.toString() + ".");
        }
        return s;
    }

    /**
     * Returns the array for the jsonObject.
     *
     * @param jsonObject to read.
     * @param key        to check.
     * @return Json file's array
     */
    public static String jsonGetArray(final JsonObject jsonObject, final String key) {
        return jsonObject.get(key).toString();
    }

    /**
     * Connects IvyRxTx to the ivyBus.
     *
     * @param ivyBus to connect IvyRxTx to.
     */
    public void ivyConnect(final String ivyBus) {
        table.setIvy(new IvyRxTx("IvyRxTx", ivyBus));
        table.getIvy().addMovedListener((robotName, x, y, theta) -> {
            table.getRobots().get(robotName).setX(Integer.parseInt(x));
            table.getRobots().get(robotName).setY(Integer.parseInt(y));
            table.getRobots().get(robotName).setTheta(Integer.parseInt(theta));
            Drawer.drawRobot(robotName, robotsView, table, ground);
        });
        table.getIvy().addTargetListener((robotName, x, y, tx, ty) -> {
            table.getRobots().get(robotName).setStartX(Double.parseDouble(x));
            table.getRobots().get(robotName).setStartY(Double.parseDouble(y));
            table.getRobots().get(robotName).setEndX(Double.parseDouble(tx));
            table.getRobots().get(robotName).setEndY(Double.parseDouble(ty));
            Drawer.drawTrajectory(table, robotName, linesView.get(robotName), ground);
        });

        // table.getIvy().addActuatorListener((robotName, actuatorName, angle)->{
        // table.getRobots().get(robotName).getActuators().
        // });
    }

    /**
     * Adds mouse events to control the robot.
     *
     * @param accordion Accordion in which are the robot controllers.
     * @param pane      Pane in which are displayed the robots.
     */
    public void mouseConnect(final Accordion accordion, final StackPane pane) {
        pane.setOnMouseClicked((final MouseEvent e) -> {
            if (accordion.getExpandedPane() != null) {
                String robotname = accordion.getExpandedPane().getText();
                ConverterTable2Ground ct2g = new ConverterTable2Ground();
                ct2g.convert(table, ground);

                // Get the Mouse location relative to the event source
                double sourceX = e.getX();
                double sourceY = e.getY();

                // Get the Mouse location relative to the table
                double tableX = ct2g.ground2TableWidth((sourceX - ct2g.getOffsetX()));
                double tableY = ct2g.ground2TableHeight((sourceY - ct2g.getOffsetY()));
                // Send the command
                System.out.println("click go_to " + robotname + " x : " + tableX + " y : " + tableY);
                table.getIvy().sendPosition(robotname, (int) tableX, (int) tableY);
            } else {
                System.out.println("No robot selected in the accordion");
                alertError("No robot selected in the accordion");
            }
        });
    }

    /**
     * Adds the keyboard controls of the robot according to the selected one in the
     * accordion.
     *
     * @param accordion Accordion in which are the controllers.
     */
    public void keyConnect(final Accordion accordion) {

        scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent event) {
                if (accordion.getExpandedPane() != null) {
                    String robotname = accordion.getExpandedPane().getText();
                    Robot<AbstractActuator> robot = table.getRobots().get(robotname);
                    final int speed = robot.getSpeed();
                    final int omega = (int) ((float) speed / ((float) robot.getWidth() / 1000 / 2));
                    if (event.getCode() == KeyCode.UP) {
                        System.out.println("up");
                        table.getIvy().sendSpeed(robotname, speed);
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        System.out.println("down");
                        table.getIvy().sendSpeed(robotname, -speed);
                    }
                    if (event.getCode() == KeyCode.LEFT) {
                        System.out.println("left");
                        table.getIvy().sendRotationSpeed(robotname, -omega);
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        System.out.println("right");
                        table.getIvy().sendRotationSpeed(robotname, omega);
                    }
                } else {
                    System.out.println("No robot selected in the accordion");
                    // alertError("No robot selected in the accordion");
                }

            }
        });
        scene.setOnKeyReleased(e -> {
            if (accordion.getExpandedPane() != null) {
                String robotname = accordion.getExpandedPane().getText();
                if ((e.getCode() == KeyCode.UP) | (e.getCode() == KeyCode.DOWN)) {
                    System.out.println("end_up_or_down");
                    table.getIvy().sendSpeed(robotname, 0);
                }
                if ((e.getCode() == KeyCode.LEFT) | (e.getCode() == KeyCode.RIGHT)) {
                    System.out.println("end_right");
                    table.getIvy().sendRotationSpeed(robotname, 0);
                }
            } else {
                System.out.println("No robot selected in the accordion");
                alertError("No robot selected in the accordion");
            }
        });
    }

    /**
     * Getter for the table.
     *
     * @return Table.
     */
    public Table getTable() {
        return table;
    }

}
