package fr.enac.sita.controbot.view.controllers;

import fr.enac.sita.controbot.communication.ActuatorListener;
import fr.enac.sita.controbot.model.robot.Motor;
import fr.enac.sita.controbot.view.View;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

/**
 * A MotorController is used to link the view with the state of the motor.
 */
public class MotorController {
    /** Robot name. */
    private String robotName;
    /** Motor to control. */
    private Motor motor;
    /** View containing the controller. */
    private View view;
    /** Property containing the direction of rotation of the motor. */
    private BooleanProperty clockwise = new SimpleBooleanProperty();
    /** Property containing the speed of the motor. */
    private IntegerProperty speed = new SimpleIntegerProperty();

    /**
     * Constructor of the MotorController.
     *
     * @param motor     Motor to control.
     * @param view      View.
     * @param box       Box.
     * @param robotname Robot's name.
     */
    public MotorController(final Motor motor, final View view, final VBox box, final String robotname) {
        this.robotName = robotname;
        this.motor = motor;
        this.view = view;
        this.clockwise.set(motor.getClockwise());
        this.speed.set(motor.getSpeed());
        addMotor(box);
    }

    /**
     * Adds the motor controller to the box.
     *
     * @param box Box.
     */
    private void addMotor(final VBox box) {
        final int labelFONTSIZE = 14;
        final int stateFONTSIZE = 11;
        final int labelTOPPADDING = 15;
        final int labelRIGHTPADDING = 0;
        final int labelBOTPADDING = 10;
        final int labelLEFTPADDING = 0;
        final int sliderMIN = 0;
        final int sliderMAX = 50;
        final int sliderSTART = 25;
        final int sliderMAJORTICKUNIT = 10;
        final int sliderMINORTICKUNIT = 9;
        final int sliderBLOCKINCREMENT = 10;
        final int commandWIDTH = 50;
        final int commandSPACING = 30;
        final int directionSPACING = 10;

        Label label = new Label(motor.getId());
        label.setFont(new Font("Arial", labelFONTSIZE));
        label.setPadding(new Insets(labelTOPPADDING, labelRIGHTPADDING, labelBOTPADDING, labelLEFTPADDING));

        HBox state = new HBox();
        Label stateSpeedTitle = new Label("Speed (m/s): ");
        stateSpeedTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label stateSpeed = new Label(Integer.toString(speed.getValue()));
        stateSpeed.setFont(new Font("Arial", stateFONTSIZE));
        state.getChildren().addAll(stateSpeedTitle, stateSpeed);
        HBox direction = new HBox();
        Label stateIsClockwiseTitle = new Label("Is clockwise: ");
        stateIsClockwiseTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label stateIsClockwise = new Label(Boolean.toString(clockwise.getValue()));
        stateIsClockwise.setFont(new Font("Arial", stateFONTSIZE));
        direction.getChildren().addAll(stateIsClockwiseTitle, stateIsClockwise);

        Label commandTitle = new Label("Command:");
        commandTitle.setFont(new Font("Arial", stateFONTSIZE));

        HBox directionButtons = new HBox();
        ToggleGroup directionGroup = new ToggleGroup();
        RadioButton clockwiseButton = new RadioButton("CW");
        RadioButton counterclockwiseButton = new RadioButton("CCW");
        clockwiseButton.setToggleGroup(directionGroup);
        counterclockwiseButton.setToggleGroup(directionGroup);
        clockwiseButton.setSelected(true);
        directionButtons.getChildren().addAll(clockwiseButton, counterclockwiseButton);
        directionButtons.setSpacing(directionSPACING);

        Slider slider = new Slider(sliderMIN, sliderMAX, sliderSTART);

        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);

        slider.setMajorTickUnit(sliderMAJORTICKUNIT);
        slider.setMinorTickCount(sliderMINORTICKUNIT);
        slider.setBlockIncrement(sliderBLOCKINCREMENT);
        slider.setSnapToTicks(true);

        HBox command = new HBox();
        TextField commandValue = new TextField();
        commandValue.setText(Double.toString(slider.getValue()));
        commandValue.setPrefWidth(commandWIDTH);
        Button commandButton = new Button("Send command");
        commandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                view.getTable().getIvy().sendMotorCommand(robotName, motor.getId(), slider.getValue(),
                        clockwiseButton.isSelected());
            }
        });
        command.getChildren().addAll(commandValue, commandButton);
        command.setSpacing(commandSPACING);

        // binding
        StringConverter<Number> sc = new NumberStringConverter();
        commandValue.textProperty().bindBidirectional(slider.valueProperty(), sc);
        stateSpeed.textProperty().bind(speed.asString());
        stateIsClockwise.textProperty().bind((clockwise.asString()));

        box.getChildren().addAll(label, state, direction, commandTitle, directionButtons, slider, command);
    }

    /**
     * Updates the controller.
     */
    public void update() {
        this.view.getTable().getIvy().addActuatorListener(robotName, motor.getId(), new ActuatorListener() {
            @Override
            public void onMySimpleEvent(final String robotname, final String actuatorName, final String var1) {
                System.out.println("Wrong use of SimpleEventListener for MotorController. " + actuatorName);
            }

            @Override
            public void onMyDoubleEvent(final String robotname, final String actuatorName, final String speedIn,
                    final String clockwiseIn) {
                Platform.runLater(() -> {
                    speed.set(Integer.parseInt(speedIn));
                    clockwise.set(Boolean.parseBoolean(clockwiseIn));
                });
            }
        });
    }
}
