package fr.enac.sita.controbot.view.controllers;

import fr.enac.sita.controbot.model.robot.AbstractActuator;
import fr.enac.sita.controbot.model.robot.Robot;
import fr.enac.sita.controbot.view.View;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * A PositionController is used to link the view with the position of the robot.
 */
public class PositionController {
    /** Robot being controlled. */
    private Robot<AbstractActuator> robot;
    /** View containing the controller. */
    private View view;
    /** Property containing the x position of the robot. */
    private StringProperty xProperty = new SimpleStringProperty();
    /** Property containing the y position of the robot. */
    private StringProperty yProperty = new SimpleStringProperty();
    /** Property containing the theta position of the robot. */
    private StringProperty thetaProperty = new SimpleStringProperty();

    /**
     * Constructor of the PositionController.
     *
     * @param robot Robot.
     * @param view  View.
     * @param box   Box.
     */
    public PositionController(final Robot<AbstractActuator> robot, final View view, final VBox box) {
        this.robot = robot;
        this.view = view;
        this.xProperty.set(Integer.toString(robot.getX()));
        this.yProperty.set(Integer.toString(robot.getY()));
        this.thetaProperty.set(Integer.toString(robot.getTheta()));
        addPosition(box);
    }

    /**
     * Adds a position controller to the box.
     *
     * @param box Box.
     */
    public void addPosition(final VBox box) {
        final int stateFONTSIZE = 11;
        final int commandWIDTH = 70;
        final int commandSPACING = 5;

        HBox statePosition = new HBox();
        Label stateTitle = new Label("Position (mm): ");
        stateTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label stateX = new Label(xProperty.getValue());
        Label mult = new Label(" x ");
        Label stateY = new Label(yProperty.getValue());
        statePosition.getChildren().addAll(stateTitle, stateX, mult, stateY);

        HBox stateOrientation = new HBox();
        Label thetaTitle = new Label("Orientation (°): ");
        thetaTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label stateTheta = new Label(xProperty.getValue());
        stateOrientation.getChildren().addAll(thetaTitle, stateTheta);

        Label commandTitle = new Label("Command:");
        commandTitle.setFont(new Font("Arial", stateFONTSIZE));

        HBox commandPosition = new HBox();
        commandPosition.setSpacing(commandSPACING);
        Label labelX = new Label("x:");
        TextField commandX = new TextField();
        commandX.setPrefWidth(commandWIDTH);
        Label labelY = new Label("y:");
        TextField commandY = new TextField();
        commandY.setPrefWidth(commandWIDTH);
        commandPosition.getChildren().addAll(labelX, commandX, labelY, commandY);

        Button commandButton = new Button("Send position command");
        commandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                // System.out.println(robot.getName() + " send go_to " + commandX.getText() + "
                // x " + commandY.getText());
                view.getTable().getIvy().sendPosition(robot.getName(), Integer.parseInt(commandX.getText()),
                        Integer.parseInt(commandY.getText()));
            }
        });

        HBox commandOrientation = new HBox();
        commandOrientation.setSpacing(commandSPACING);
        Label labelTheta = new Label("theta:");
        TextField commandTheta = new TextField();
        commandTheta.setPrefWidth(commandWIDTH);
        commandOrientation.getChildren().addAll(labelTheta, commandTheta);

        Button commandThetaButton = new Button("Send orientation command");
        commandThetaButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                // System.out.println(robot.getName() + " send go_theta " +
                // commandTheta.getText());
                view.getTable().getIvy().sendTheta(robot.getName(), Integer.parseInt(commandTheta.getText()));
            }
        });

        box.getChildren().addAll(statePosition, stateOrientation, commandTitle, commandPosition, commandButton,
                commandOrientation, commandThetaButton);

        // binding
        // ChangeListener<String> listenerX = (obs, oldStatus, newStatus) ->
        // stateX.setText(newStatus);
        // xProperty.addListener(listenerX);
        // ChangeListener<String> listenerY = (obs, oldStatus, newStatus) ->
        // stateY.setText(newStatus);
        // yProperty.addListener(listenerY);

        stateX.textProperty().bind(xProperty);
        stateY.textProperty().bind(yProperty);
        stateTheta.textProperty().bind(thetaProperty);
    }

    /**
     * Updates the controller.
     */
    public void update() {
        this.view.getTable().getIvy().addMovedListener((robotname, x, y, theta) -> {
            if (robotname.equals(robot.getName())) {
                Platform.runLater(() -> {
                    this.xProperty.set(x);
                    this.yProperty.set(y);
                    this.thetaProperty.set(theta);
                });
                // System.out.println("x property : "+this.xProperty+", y property :
                // "+this.yProperty);
            }
        });
    }

    /**
     * Getter for the robot.
     *
     * @return Robot.
     */
    public Robot<AbstractActuator> getRobot() {
        return robot;
    }

    /**
     * Getter for the view.
     *
     * @return View.
     */
    public View getView() {
        return view;
    }

    /**
     * Getter for the x position Property.
     *
     * @return xProperty.
     */
    public StringProperty getX() {
        return xProperty;
    }

    /**
     * Getter for the y position Property.
     *
     * @return yProperty.
     */
    public StringProperty getY() {
        return yProperty;
    }

}
