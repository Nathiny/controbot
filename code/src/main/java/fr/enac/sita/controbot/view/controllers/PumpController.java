package fr.enac.sita.controbot.view.controllers;

import fr.enac.sita.controbot.communication.ActuatorListener;
import fr.enac.sita.controbot.model.robot.Pump;
import fr.enac.sita.controbot.view.View;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

/**
 * A PumpController is used to link the view with the state of the pump.
 */
public class PumpController {
    /** Robot name. */
    private String robotName;
    /** Pump to control. */
    private Pump pump;
    /** View containing the controller. */
    private View view;
    /** Property containing if the pump catched something. */
    private BooleanProperty sthCatched = new SimpleBooleanProperty();
    /** Property containing the current power of the pump. */
    private IntegerProperty power = new SimpleIntegerProperty();

    /**
     * Constructor of the PumpController.
     *
     * @param pump      Pump to control.
     * @param view      View.
     * @param box       Box.
     * @param robotname Robot's name.
     */
    public PumpController(final Pump pump, final View view, final VBox box, final String robotname) {
        this.robotName = robotname;
        this.pump = pump;
        this.view = view;
        this.sthCatched.set(pump.getSthCatched());
        this.power.set(pump.getPower());
        addPump(box);
    }

    /**
     * Adds a pump controller to the box.
     *
     * @param box Box.
     */
    private void addPump(final VBox box) {
        final int labelFONTSIZE = 14;
        final int stateFONTSIZE = 11;
        final int labelTOPPADDING = 15;
        final int labelRIGHTPADDING = 0;
        final int labelBOTPADDING = 10;
        final int labelLEFTPADDING = 0;
        final int commandSPACING = 30;
        final int sliderMIN = 0;
        final int sliderMAX = 50;
        final int sliderSTART = 25;
        final int sliderMAJORTICKUNIT = 10;
        final int sliderMINORTICKUNIT = 9;
        final int sliderBLOCKINCREMENT = 10;
        final int commandWIDTH = 50;

        Label label = new Label(pump.getId());
        label.setFont(new Font("Arial", labelFONTSIZE));
        label.setPadding(new Insets(labelTOPPADDING, labelRIGHTPADDING, labelBOTPADDING, labelLEFTPADDING));

        HBox state = new HBox();
        Label statePowerTitle = new Label("Power (m/s): ");
        statePowerTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label statePower = new Label(Integer.toString(power.getValue()));
        statePower.setFont(new Font("Arial", stateFONTSIZE));
        state.getChildren().addAll(statePowerTitle, statePower);

        HBox stateSthCatched = new HBox();
        Label stateSthCatchedTitle = new Label("Something is catched: ");
        Label stateSthCatchedValue = new Label(Boolean.toString(sthCatched.getValue()));
        stateSthCatchedTitle.setFont(new Font("Arial", stateFONTSIZE));
        stateSthCatchedValue.setFont(new Font("Arial", stateFONTSIZE));
        stateSthCatched.getChildren().addAll(stateSthCatchedTitle, stateSthCatchedValue);

        Label commandTitle = new Label("Command:");
        commandTitle.setFont(new Font("Arial", stateFONTSIZE));

        Slider slider = new Slider(sliderMIN, sliderMAX, sliderSTART);

        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);

        slider.setMajorTickUnit(sliderMAJORTICKUNIT);
        slider.setMinorTickCount(sliderMINORTICKUNIT);
        slider.setBlockIncrement(sliderBLOCKINCREMENT);
        slider.setSnapToTicks(true);

        HBox command = new HBox();
        TextField commandValue = new TextField();
        commandValue.setText(Double.toString(slider.getValue()));
        commandValue.setPrefWidth(commandWIDTH);
        Button commandButton = new Button("Send command");
        commandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                view.getTable().getIvy().sendPumpCommand(robotName, pump.getId(), slider.getValue());
            }
        });
        command.getChildren().addAll(commandValue, commandButton);
        command.setSpacing(commandSPACING);

        // binding
        StringConverter<Number> sc = new NumberStringConverter();
        commandValue.textProperty().bindBidirectional(slider.valueProperty(), sc);
        statePower.textProperty().bind(power.asString());
        stateSthCatchedValue.textProperty().bind(sthCatched.asString());

        box.getChildren().addAll(label, state, stateSthCatched, commandTitle, slider, command);
    }

    /**
     * Updates the controller.
     */
    public void update() {
        this.view.getTable().getIvy().addActuatorListener(robotName, pump.getId(), new ActuatorListener() {
            @Override
            public void onMySimpleEvent(final String robotname, final String actuatorName, final String var1) {
                System.out.println("Wrong use of SimpleEventListener for MotorController. " + actuatorName);
            }

            @Override
            public void onMyDoubleEvent(final String robotname, final String actuatorName, final String powerIn,
                    final String sthCatchedIn) {
                Platform.runLater(() -> {
                    power.set(Integer.parseInt(powerIn));
                    sthCatched.set(Boolean.parseBoolean(sthCatchedIn));
                });
            }
        });
    }
}
