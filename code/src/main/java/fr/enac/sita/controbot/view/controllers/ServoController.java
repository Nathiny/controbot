package fr.enac.sita.controbot.view.controllers;

import fr.enac.sita.controbot.communication.ActuatorListener;
import fr.enac.sita.controbot.model.robot.Servo;
import fr.enac.sita.controbot.view.View;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

/**
 * A ServoController is used to link the view with the state of the servo.
 */
public class ServoController {
    /** Robot name. */
    private String robotName;
    /** Servo to control. */
    private Servo servo;
    /** View containing the controller. */
    private View view;
    /** Property containing the state of the servo (angle). */
    private DoubleProperty angle = new SimpleDoubleProperty();

    /**
     * Constructor of the ServoController.
     *
     * @param servo     Servo to control.
     * @param view      View.
     * @param box       Box.
     * @param robotname Robot's name.
     */
    public ServoController(final Servo servo, final View view, final VBox box, final String robotname) {
        this.robotName = robotname;
        this.servo = servo;
        this.view = view;
        this.angle.set(servo.getAngle());
        addServo(box);
    }

    /**
     * Returns the servo that is controlled by the controller.
     *
     * @return Controller's servo.
     */
    public Servo getServo() {
        return servo;
    }

    /**
     * Adds the servo controller to the box.
     *
     * @param box Box.
     */
    private void addServo(final VBox box) {
        final int labelFONTSIZE = 14;
        final int stateFONTSIZE = 11;
        final int labelTOPPADDING = 15;
        final int labelRIGHTPADDING = 0;
        final int labelBOTPADDING = 10;
        final int labelLEFTPADDING = 0;
        final int sliderMIN = 0;
        final int sliderMAX = 180;
        final int sliderSTART = 90;
        final int sliderMAJORTICKUNIT = 20;
        final int sliderMINORTICKUNIT = 19;
        final int sliderBLOCKINCREMENT = 10;
        final int commandWIDTH = 50;
        final int commandSPACING = 30;

        Label label = new Label(servo.getId());
        label.setFont(new Font("Arial", labelFONTSIZE));
        label.setPadding(new Insets(labelTOPPADDING, labelRIGHTPADDING, labelBOTPADDING, labelLEFTPADDING));

        HBox state = new HBox();
        Label stateTitle = new Label("Position (°): ");
        stateTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label stateAngle = new Label(Double.toString(angle.getValue()));
        stateAngle.setFont(new Font("Arial", stateFONTSIZE));
        state.getChildren().addAll(stateTitle, stateAngle);

        Label commandTitle = new Label("Command:");
        commandTitle.setFont(new Font("Arial", stateFONTSIZE));

        Slider slider = new Slider(sliderMIN, sliderMAX, sliderSTART);

        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);

        slider.setMajorTickUnit(sliderMAJORTICKUNIT);
        slider.setMinorTickCount(sliderMINORTICKUNIT);
        slider.setBlockIncrement(sliderBLOCKINCREMENT);
        slider.setSnapToTicks(true);

        HBox command = new HBox();
        TextField commandValue = new TextField();
        commandValue.setText(Double.toString(slider.getValue()));
        commandValue.setPrefWidth(commandWIDTH);
        Button commandButton = new Button("Send command");
        commandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                view.getTable().getIvy().sendServoCommand(robotName, servo.getId(), slider.getValue());
            }
        });
        command.getChildren().addAll(commandValue, commandButton);
        command.setSpacing(commandSPACING);

        // binding
        StringConverter<Number> sc = new NumberStringConverter();
        commandValue.textProperty().bindBidirectional(slider.valueProperty(), sc);
        stateAngle.textProperty().bind(angle.asString());

        box.getChildren().addAll(label, state, commandTitle, slider, command);
    }

    /**
     * Updates the controller.
     */
    public void update() {
        this.view.getTable().getIvy().addActuatorListener(robotName, servo.getId(), new ActuatorListener() {
            @Override
            public void onMySimpleEvent(final String robotname, final String actuatorName, final String angleIn) {
                Platform.runLater(() -> {
                    angle.set(Double.parseDouble(angleIn));
                });
            }

            @Override
            public void onMyDoubleEvent(final String robotname, final String actuatorName, final String var1,
                    final String var2) {
                System.out.println("Wrong use of DoubleEventListener for ServoController. " + actuatorName);
            }
        });
    }
}
