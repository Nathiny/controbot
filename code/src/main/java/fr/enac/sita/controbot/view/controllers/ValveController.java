package fr.enac.sita.controbot.view.controllers;

import fr.enac.sita.controbot.communication.ActuatorListener;
import fr.enac.sita.controbot.model.robot.Valve;
import fr.enac.sita.controbot.view.View;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * ValveController is used to link the view with the state of the valve.
 */
public class ValveController {
    /** Robot name. */
    private String robotName;
    /** Valve to control. */
    private Valve valve;
    /** View containing the controller. */
    private View view;
    /** Property containing the state of the valve (open or close). */
    private BooleanProperty open = new SimpleBooleanProperty();

    /**
     * Constructor of the ValveController.
     *
     * @param valve     Valve.
     * @param view      View.
     * @param box       Box.
     * @param robotname Robot's name.
     */
    public ValveController(final Valve valve, final View view, final VBox box, final String robotname) {
        this.robotName = robotname;
        this.valve = valve;
        this.view = view;
        this.open.set(valve.getOpen());
        addValve(box);
    }

    /**
     * Adds a valve controller to the box.
     *
     * @param box Box.
     */
    private void addValve(final VBox box) {
        final int labelFONTSIZE = 14;
        final int stateFONTSIZE = 11;
        final int labelTOPPADDING = 15;
        final int labelRIGHTPADDING = 0;
        final int labelBOTPADDING = 10;
        final int labelLEFTPADDING = 0;
        final int commandSPACING = 30;

        Label label = new Label(valve.getId());
        label.setFont(new Font("Arial", labelFONTSIZE));
        label.setPadding(new Insets(labelTOPPADDING, labelRIGHTPADDING, labelBOTPADDING, labelLEFTPADDING));

        HBox state = new HBox();
        Label stateTitle = new Label("Is open: ");
        stateTitle.setFont(new Font("Arial", stateFONTSIZE));
        Label stateOpen = new Label(Boolean.toString(open.getValue()));
        stateOpen.setFont(new Font("Arial", stateFONTSIZE));
        state.getChildren().addAll(stateTitle, stateOpen);

        Label commandTitle = new Label("Command:");
        commandTitle.setFont(new Font("Arial", stateFONTSIZE));

        HBox command = new HBox();
        Button openButton = new Button("Open");
        openButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                view.getTable().getIvy().sendValveCommand(robotName, valve.getId(), 1);
            }
        });
        Button closeButton = new Button("Close");
        closeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent e) {
                view.getTable().getIvy().sendValveCommand(robotName, valve.getId(), 0);
            }
        });
        command.getChildren().addAll(openButton, closeButton);
        command.setSpacing(commandSPACING);

        // binding
        stateOpen.textProperty().bind(open.asString());

        box.getChildren().addAll(label, state, commandTitle, command);
    }

    /**
     * Updates the controller.
     */
    public void update() {
        this.view.getTable().getIvy().addActuatorListener(robotName, valve.getId(), new ActuatorListener() {
            @Override
            public void onMySimpleEvent(final String robotname, final String actuatorName, final String openIn) {
                Platform.runLater(() -> {
                    open.set(Boolean.parseBoolean(openIn));
                });
            }

            @Override
            public void onMyDoubleEvent(final String robotname, final String actuatorName, final String var1,
                    final String var2) {
                System.out.println("Wrong use of DoubleEventListener for ServoController. " + actuatorName);
            }
        });
    }
}
