/**
 * Package containing all the controllers to control the robot and its
 * actuators.
 */
package fr.enac.sita.controbot.view.controllers;
