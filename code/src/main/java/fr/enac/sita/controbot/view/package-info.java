/**
 * Package managing the conversion from the model to the view done with JavaFX.
 */
package fr.enac.sita.controbot.view;
