package fr.enac.sita.controbot.model.robot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MotorTests {

    private Motor motor;

    private void tests(String id, int speed, Boolean clockwise) {
        assertEquals(id, motor.getId(), "The id of the pump should be equal to '" + id + "'");
        assertEquals(speed, motor.getSpeed(), "The power should be equal to '" + speed + "'");
        assertEquals(clockwise, motor.getClockwise(), "The sthCatched should be equal to '" + clockwise + "'");
    }

    @DisplayName("Getter test")
    @ParameterizedTest
    @CsvSource({ "motorId, 12, false", "motorIda, 12, true", "motorIds, 120, false", "motorId, 200, true" })
    void getterTest(String id, int speed, Boolean clockwise) {
        motor = new Motor(id, speed, clockwise);
        tests(id, speed, clockwise);
    }

    @DisplayName("Setter test")
    @ParameterizedTest
    @CsvSource({ "motorId, 12, false", "motorIda, 12, true", "motorIds, 120, false", "motorId, 200, true" })
    void setterTest(String id, int speed, Boolean clockwise) {
        motor = new Motor(id);
        motor.setSpeed(speed);
        motor.setClockwise(clockwise);
        tests(id, speed, clockwise);
    }
}
