package fr.enac.sita.controbot.model.robot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PumpTests {
    private Pump pump;

    void tests(String id, int power, Boolean sthCatched) {
        assertEquals(id, pump.getId(), "The id of the pump should be equal to '" + id + "'");
        assertEquals(power, pump.getPower(), "The power should be equal to '" + power + "'");
        assertEquals(sthCatched, pump.getSthCatched(), "The sthCatched should be equal to '" + sthCatched + "'");
    }

    @DisplayName("Getter test")
    @ParameterizedTest
    @CsvSource({ "pumpId, 12, true", "pumpId0, 5, true", "pumpId0, 12, false", "pumpId, 5, false" })
    void getterTest(String id, int power, Boolean sthCatched) {
        pump = new Pump(id, power, sthCatched);
        tests(id, power, sthCatched);
    }

    @DisplayName("Setter test")
    @ParameterizedTest
    @CsvSource({ "pumpId, 6, false", "pumpId0, 5, true", "pumpId0, 12, false", "pumpId, 5, false" })
    void setterTest(String id, int power, Boolean sthCatched) {
        pump = new Pump(id, 0, true);
        pump.setPower(power);
        pump.setSthCatched(sthCatched);
        getterTest(id, power, sthCatched);
    }
}
