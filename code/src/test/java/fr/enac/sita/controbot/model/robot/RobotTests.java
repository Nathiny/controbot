package fr.enac.sita.controbot.model.robot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RobotTests {

    private Robot<AbstractActuator> robot;

    private void tests(String name, int x, int y, int battery, int width, int height, int speed) {
        assertEquals(name, robot.getName(), "The name of the pump should be equal to '" + name + "'");
        assertEquals(speed, robot.getSpeed(), "The power should be equal to '" + speed + "'");
        assertEquals(x, robot.getX(), "The x should be equal to '" + x + "'");
        assertEquals(y, robot.getY(), "The y should be equal to '" + y + "'");
        assertEquals(battery, robot.getBattery(), "The battery should be equal to '" + battery + "'");
        assertEquals(width, robot.getWidth(), "The width should be equal to '" + width + "'");
        assertEquals(height, robot.getHeight(), "The height should be equal to '" + height + "'");

    }

    @DisplayName("Getter test")
    @ParameterizedTest
    @CsvSource({ "robotName, 120, 1200, 75, 300, 200, 200", "robotName, 1200, 100, 5, 30, 200, 20",
            "robotNameA, 120, 1207, 7, 300, 700, 100", "robotName, 120, 1200, 75, 300, 200, 200" })
    void getterTest(String name, int x, int y, int battery, int width, int height, int speed) {
        robot = new Robot<>(name, x, y, battery, width, height, speed);
        tests(name, x, y, battery, width, height, speed);

    }

    @DisplayName("Setter test")
    @ParameterizedTest
    @CsvSource({ "robotName, 120, 1200, 75, 300, 200, 200, 1200, 55, 112, 156",
            "robotName, 1200, 100, 5, 30, 200, 20, 1200, 55, 112, 156",
            "robotNameA, 120, 1207, 7, 300, 30, 100, 1200, 55, 112, 156",
            "robotName, 120, 1200, 75, 300, 200, 200, 100, 505, 11, 156" })
    void setterTest(String name, int x, int y, int battery, int width, int height, int speed, double startX,
            double startY, double endX, double endY) {
        robot = new Robot<>(name, x, y, battery, width, height, speed);
        robot.setX(x);
        robot.setY(y);
        robot.setStartX(startX);
        robot.setStartY(startY);
        robot.setEndX(endX);
        robot.setEndY(endY);
        tests(name, x, y, battery, width, height, speed);
        assertEquals(startX, robot.getStartX(), "The startX should be equal to '" + startX + "'");
        assertEquals(startY, robot.getStartY(), "The startY should be equal to '" + startY + "'");
        assertEquals(endX, robot.getEndX(), "The endX should be equal to '" + endX + "'");
        assertEquals(endY, robot.getEndY(), "The endY should be equal to '" + endY + "'");

    }
}
