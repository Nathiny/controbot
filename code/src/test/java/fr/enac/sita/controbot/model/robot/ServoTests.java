package fr.enac.sita.controbot.model.robot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ServoTests {

    private Servo servo;

    private void tests(String id, int angle) {
        assertEquals(id, servo.getId(), "The id of the pump should be equal to '" + id + "'");
        assertEquals(angle, servo.getAngle(), "The power should be equal to '" + angle + "'");
    }

    @DisplayName("Getter test")
    @ParameterizedTest
    @CsvSource({ "servoId, 12", "servoIda, 52", "servoqIdq, 0", "servoIds, 92", "servoId, 102" })
    void getterTest(String id, int angle) {
        servo = new Servo(id, angle);
        tests(id, angle);
    }

    @DisplayName("Setter test")
    @ParameterizedTest
    @CsvSource({ "servoId, 12", "servoIda, 52", "servoqIdq, 0", "servoIds, 92", "servoId, 102" })
    void setterTest(String id, int angle) {
        servo = new Servo(id);
        servo.setAngle(angle);
        tests(id, angle);
    }
}
