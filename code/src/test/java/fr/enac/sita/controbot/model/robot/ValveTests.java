package fr.enac.sita.controbot.model.robot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ValveTests {
    private Valve valve;

    void tests(String id, Boolean opened) {
        assertEquals(id, valve.getId(), "The id of the valve should be equal to '" + id + "'");
        assertEquals(opened, valve.getOpen(), "The opened value should be equal to '" + opened + "'");
    }

    @DisplayName("Getter test")
    @ParameterizedTest
    @CsvSource({ "valveId, true", "valveId0, true", "valveId0, false", "valveId, false" })
    void getterTest(String id, Boolean opened) {
        valve = new Valve(id, opened);
        tests(id, opened);
    }

    @DisplayName("Setter test")
    @ParameterizedTest
    @CsvSource({ "valveId, false", "valveId0, true", "valveId0, false", "valveId, false" })
    void setterTest(String id, Boolean opened) {
        valve = new Valve(id, true);
        valve.setOpen(opened);
        getterTest(id, opened);
    }
}
