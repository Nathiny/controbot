# Livrables

Tous les livrables sont dans ce dossier. Les fichiers sont compilés avec jdk-openjdk13 (pour java 13 et java 11). Seul le code source, les fichiers de tests et le fichier expliquant comment compiler le programme sont dans le dossier code.
Un fichier expliquant le programme et redirigeant vers d'autres pages à lire (compilation du programme et organisation des classes) est situé à la racine du projet (pour une meilleure visualition, suivez ce lien https://gitlab.com/Nathiny/controbot/-/blob/master/readme.md).
