# Controbot

Controbot is a java program to control your robot using the [Ivy software bus](https://www.eei.cena.fr/products/ivy/).

GitLab repository is here: [https://gitlab.com/Nathiny/controbot](https://gitlab.com/Nathiny/controbot)

It can send position command to your robot by using the mouse or speed command using the keyboard. It can also command the actuators of your robot.
It can launch pre-defined action sequences really easily. 

[![interface](img/interface.png)](https://streamable.com/obqdrk "Interface")

## Configuration files

### Table

To define a table, make a json file using this format:

```json
{
    "name": "Sail The World",
    "ground": "SailTheWorld.png",
    "obstacles": "SailTheWorld.csv",
    "width": 3000,
    "height": 2000
}
```

Place the image and obstacle files in the same folder with the file.
The obstacle file needs to follow this format (name;shape;x;y;width;height):

```csv
obs1;rectangle;1489;1700;22;300
obs2;rectangle;899;1850;22;150
obs2;rectangle;2089;1850;22;150
```

### Robot

To define a robot, make a json file using this format:

```json
{
    "name": "Edgar",
    "x": 1000,
    "y": 1500,
    "theta": 45,
    "battery": 90,
    "color": "GREEN",
    "width": 200,
    "height": 250,
    "speed": 1,
    "actuators": [
        {
            "id": "servo1",
            "type": "servo",
            "angle": 0
        },
        {
            "id": "motor1",
            "type": "motor",
            "speed": 0,
            "clockwise": true
        },
        {
            "id": "pump1",
            "type": "pump",
            "power": 5,
            "sthCatched": false
        },
        {
            "id": "valve1",
            "type": "valve",
            "open": true
        }
    ]
}
```

### Sequences

To define a sequence, make a json file using this format:

```json
{
    "id": "EdgarSequence",
    "robot": "Edgar",
    "actions": [
        {
            "id": "init",
            "when": [
                "init",
                ""
            ],
            "action": "go_to",
            "params": [
                "1000",
                "1000",
                "90"
            ]
        },
        {
            "id": "2ndGoTo",
            "when": [
                "after",
                "init"
            ],
            "action": "go_to",
            "params": [
                "1500",
                "1500",
                "190"
            ]
        },
        {
            "id": "setServo",
            "when": [
                "while",
                "2ndGoTo"
            ],
            "action": "servo1 set_angle",
            "params": [
                "90"
            ]
        }
    ]
}
```



### Configuration files

Configuration files allows you to load a table, an ivy bus, robots and sequences easily.
To define a configuration, make a json file using this format:
```json
{
    "name": "Sail The World - robot a + sequences",
    "table": "../tables/SailTheWorld.json",
    "ivyBus": "192.168.0.0:25565",
    "robots": [
        "../robots/a/a.json"
    ],
    "sequences": [
        "../robots/a/aSequence.json",
        "../robots/a/aSequence2.json"
    ]
}
```
All paths are relative.

## Using Controbot

Using Controbot is pretty straightforward. Load everything you want, then select the robot you want to control in the right pane. You can now control this robot using the arrow keys or the mouse.

To see how to compile and launch Controbot, go to [this page](code/README.md "readme").


If you want to contribute to this project, go to [this page](uml/uml.md "readme") to learn more about the program's structure.


