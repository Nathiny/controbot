# Structure of the program

## General structure

<section class="hierarchy">
<h3 title="Class Hierarchy">Class Hierarchy</h2>
<ul>
<li class="circle">java.lang.<a href="https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/lang/Object.html?is-external=true" title="class or interface in java.lang" class="externalLink"><span class="typeNameLink">Object</span></a>
<ul>
<li class="circle">fr.enac.sita.controbot.model.robot.AbstractActuator</span></a>
<ul>
<li class="circle">fr.enac.sita.controbot.model.robot.Motor</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.robot.Pump</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.robot.Servo</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.robot.Valve</span></a></li>
</ul>
</li>
<li class="circle">fr.enac.sita.controbot.model.table.AbstractShape</span></a>
<ul>
<li class="circle">fr.enac.sita.controbot.model.table.SCircle</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.table.SRectangle</span></a></li>
</ul>
</li>
<li class="circle">fr.enac.sita.controbot.actions.Action</span></a></li>
<li class="circle">fr.enac.sita.controbot.actions.Runnable</a></li>
<li class="circle">javafx.application.Application
<ul>
<li class="circle">fr.enac.sita.controbot.view.View</span></a></li>
</ul>
</li>
<li class="circle">fr.enac.sita.controbot.view.ConverterTable2Ground</span></a></li>
<li class="circle">fr.enac.sita.controbot.view.Drawer</span></a></li>
<li class="circle">fr.enac.sita.controbot.communication.IvyRxTx</span></a></li>
<li class="circle">fr.enac.sita.controbot.view.Loader</span></a></li>
<li class="circle">fr.enac.sita.controbot.Main</span></a></li>
<li class="circle">fr.enac.sita.controbot.view.controllers.MotorController</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.table.Obstacle</span></a></li>
<li class="circle">fr.enac.sita.controbot.view.controllers.PositionController</span></a></li>
<li class="circle">fr.enac.sita.controbot.view.controllers.PumpController</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.robot.Robot</span></a>&lt;T&gt;</li>
<li class="circle">fr.enac.sita.controbot.view.controllers.ServoController</span></a></li>
<li class="circle">fr.enac.sita.controbot.model.table.Table</span></a></li>
<li class="circle">java.util.<a href="https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/TimerTask.html?is-external=true" title="class or interface in java.util" class="externalLink"><span class="typeNameLink">TimerTask</span></a> (implements java.lang.<a href="https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/lang/Runnable.html?is-external=true" title="class or interface in java.lang" class="externalLink">Runnable</a>)
<ul>
<li class="circle">fr.enac.sita.controbot.communication.RobotUpdateTask</span></a></li>
</ul>
</li>
<li class="circle">fr.enac.sita.controbot.view.controllers.ValveController</span></a></li>
</ul>
</li>
</ul>
</section>
<section class="hierarchy">
<h3 title="Interface Hierarchy">Interface Hierarchy</h2>
<ul>
<li class="circle">fr.enac.sita.controbot.communication.ActuatorListener</span></a></li>
<li class="circle">fr.enac.sita.controbot.communication.MovedListener</span></a></li>
<li class="circle">fr.enac.sita.controbot.communication.TargetListener</span></a></li>
<li class="circle">fr.enac.sita.controbot.communication.TrajectoryListener</span></a></li>
</ul>
</section>


![interface](class_out/fullPruned/fullPruned.svg)


## Packages structure

### Actions

![interface](class_out/actions/actions.svg)


### Communication

![interface](class_out/communication/communication.svg)


### Controbot

![interface](class_out/controbot/controbot.svg)

### Controllers

![interface](class_out/controllers/controllers.svg)

### Robot

![interface](class_out/robot/robot.svg)

### Table

![interface](class_out/table/table.svg)

### View

![interface](class_out/view/view.svg)